@extends('frontend.layouts.main') 
@section('nav') 
@include('frontend.partials.nav') 
@stop 
@section('page_title')
@include('frontend.partials.page_title') 
@stop 
@section('main_container')
<div class="row">
    <div class="posts-wrap">
        @foreach($articles as $article)
        <div class="col-md-4 article-wrap">
            <article class="post type2">
                <div class="entry-thumb"> <img style="height: 200px; width: 100%;" src="{{ $article->image }}" class="img-responsive" alt="">
                    <div class="image-overlay"></div> <a href="{{ $article->image }}" data-rel="prettyPhoto" class="ima-zoom"><i class="fa fa-plus"></i></a> </div>
                <!-- /.entry-thumb -->
                <div class="entry-wrap">
                    <header class="entry-header">
                        <h1 class="entry-title"><a href="article/{{ $article->slug }}" rel="bookmark">{{ $article->title }}</a></h1> </header>
                    <!-- /.entry-header -->
                    <div class="entry-content">
                        {!! str_limit($article->description, $limit = 100, $end = '...') !!}
                    </div>
                    <!-- /.entry-content -->
                    <div class="entry-meta"> <span class="post-date"><i class="fa fa-clock-o"></i> <a href="#" title="6:17 pm">{{ date("d F Y",strtotime($article->created_at)) }}</a></span> </div>
                    <!-- /.entry-meta -->
                </div>
                <!-- /.entry-wrap -->
            </article>
            <!-- /.post -->
        </div>
        @endforeach
        
    </div>
</div>
@stop 