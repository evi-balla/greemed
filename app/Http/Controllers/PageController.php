<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \App\Product as Product;
use \App\Article as Article;
use \App\Category as Category;
use \App\GalleryImages as GalleryImages;
use Lang;
use Exception;
use View;

class PageController extends BaseController
{
    
    
    public function homePage()
    {
        try {
            
            $pageTitle = Lang::get('translations.page.home');
            
            $pageName = 'home';
            
            $products = Product::take(10)->orderBy('created_at', 'DESC')->get();
            
            $articles = Article::where('featured', '=', 1)->take(3)->orderBy('created_at', 'DESC')->get();
            
            $data = [
                'pageTitle' => $pageTitle,
                'pageName' => $pageName,
                'products' => $products,
                'articles' => $articles
            ];
            
            return view('frontend.index', $data);
            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    
    
    public function aboutPage()
    {
        try {
            
            $pageTitle = Lang::get('translations.page.about');
            $pageName = 'about';
            $products = Product::take(10)->orderBy('created_at', 'DESC')->get();
            
            $data = [
                'pageTitle' => $pageTitle,
                'pageName' => $pageName,
                'products' => $products,
            ];
            
            return view('frontend.about', $data);
            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    
    
    public function contactPage()
    {
        try {
            
            $pageTitle = Lang::get('translations.page.contact');
            $pageName = 'contact';
            $products = Product::take(10)->orderBy('created_at', 'DESC')->get();
            
            $data = [
                'pageTitle' => $pageTitle,
                'pageName' => $pageName,
                'products' => $products
            ];
            
            return view('frontend.contact', $data);
            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    
    
    public function qualityPage()
    {
        try {
            
            $pageTitle = Lang::get('translations.page.quality');
            $pageName = 'quality';
            $products = Product::take(10)->orderBy('created_at', 'DESC')->get();
            
            $data = [
                'pageTitle' => $pageTitle,
                'pageName' => $pageName,
                'products' => $products
            ];
            
            return view('frontend.quality', $data);
            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    
    
    public function logisticsPage()
    {
        try {
            
            $pageTitle = Lang::get('translations.page.logistics');
            $pageName = 'logistic';
            $products = Product::take(10)->orderBy('created_at', 'DESC')->get();
            
            $data = [
                'pageTitle' => $pageTitle,
                'pageName' => $pageName,
                'products' => $products
            ];
            
            return view('frontend.logistics', $data);
            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    
    
    public function newsPage()
    {
        try {
            
            $pageTitle = Lang::get('translations.page.news');
            $pageName = 'news';
            $products = Product::take(10)->orderBy('created_at', 'DESC')->get();
            $articles = Article::all();
            
            $data = [
                'pageTitle' => $pageTitle,
                'pageName' => $pageName,
                'products' => $products,
                'articles' => $articles
            ];
            
            return view('frontend.news', $data);
            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    
    
    public function productsPage()
    {
        try {
            
            $pageTitle = Lang::get('translations.page.products');
            $pageName = 'products';
            $products = Product::take(10)->orderBy('created_at', 'DESC')->get();
            $first_category = Category::first();
            $categories = Category::all();
            
            $data = [
                'pageTitle' => $pageTitle,
                'pageName' => $pageName,
                'products' => $products,
                'categories' => $categories,
                'first_category' => $first_category
            ];
            
            return view('frontend.products', $data);
            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    
    
    public function galleryPage()
    {
        try {
            
            $pageTitle = Lang::get('translations.page.gallery');
            $pageName = 'gallery';
            $products = Product::take(10)->orderBy('created_at', 'DESC')->get();
            $gallery_images = GalleryImages::all();
            
            $data = [
                'pageTitle' => $pageTitle,
                'pageName' => $pageName,
                'products' => $products,
                'gallery_images' => $gallery_images,
            ];
            
            return view('frontend.gallery', $data);
            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    
    public function productPage($slug)
    {
            
        $pageTitle = Lang::get('translations.page.products');
        $pageName = 'product';
        $product = Product::findBySlug($slug);
        
        if(!$product) {
            return view('errors.503');
        }
        
        $products = Product::take(10)->orderBy('created_at', 'DESC')->get();
        $relatedProducts = Product::where('sub_category_id', '=', $product->subCategory->id)
            ->where('id', '<>', $product->id)
            ->take(10)
            ->get();
        
        $data = [
            'pageTitle' => $pageTitle,
            'pageName' => $pageName,
            'product' => $product,
            'products' => $products,
            'relatedProducts' => $relatedProducts
        ];
        
        return view('frontend.product-item', $data);
    }
    
    public function articlePage($slug)
    {
            
        $article = Article::findBySlug($slug);
        $pageTitle = Lang::get('translations.page.news');
        $pageName = 'article';
        
        
        if(!$article) {
            return view('errors.503');
        }
        
        $products = Product::take(10)->orderBy('created_at', 'DESC')->get();
        
        $data = [
            'pageTitle' => $pageTitle,
            'pageName' => $pageName,
            'article' => $article,
            'products' => $products
        ];
        
        return view('frontend.article-item', $data);
    }
    
    public function errorsPage()
    {
        try {
            
            $pageTitle = Lang::get('translations.page.errors');
            $pageName = 'errorsPg';

            $data = [
                'pageTitle' => $pageTitle,
                'pageName' => $pageName
                ];
            
            return view('errors.404', $data);
            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
