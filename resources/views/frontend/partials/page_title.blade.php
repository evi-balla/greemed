@if ( $pageName === 'about')
<header class="archive-header" style="background-image: radial-gradient(circle, rgba(255, 255, 255, 0.07) 1%, rgba(0, 0, 0, 0.5) 100%), url('/img/banners/slides_about-us.jpg')">
@elseif ( $pageName === 'contact' )
<header class="archive-header" style="background-image: radial-gradient(circle, rgba(255, 255, 255, 0.07) 1%, rgba(0, 0, 0, 0.5) 100%), url('/img/banners/slides_kontakt.jpg')">
@elseif ( $pageName === 'news' )
<header class="archive-header" style="background-image: radial-gradient(circle, rgba(255, 255, 255, 0.07) 1%, rgba(0, 0, 0, 0.5) 100%), url('/img/banners/slides_news.jpg')">
@elseif ( $pageName === 'quality' )
<header class="archive-header" style="background-image: radial-gradient(circle, rgba(255, 255, 255, 0.07) 1%, rgba(0, 0, 0, 0.5) 100%), url('/img/banners/slides_quality.jpg')">
@elseif ( $pageName === 'gallery' )
<header class="archive-header" style="background-image: radial-gradient(circle, rgba(255, 255, 255, 0.07) 1%, rgba(0, 0, 0, 0.5) 100%), url('/img/banners/slides_gallery2.jpg')">
@elseif ( $pageName === 'products' )
<header class="archive-header" style="background-image: radial-gradient(circle, rgba(255, 255, 255, 0.07) 1%, rgba(0, 0, 0, 0.5) 100%), url('/img/banners/slides_produkts.jpg')">
@elseif ( $pageName === 'product' )
<header class="archive-header" style="background-image: radial-gradient(circle, rgba(255, 255, 255, 0.07) 1%, rgba(0, 0, 0, 0.5) 100%), url('/img/banners/slides_produkts.jpg')">
@else
<header class="archive-header">
@endif
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="archive-title">{{ $pageTitle }}</h4> </div>
            <!-- /.col-sm-6 -->
            <div class="col-xs-6 hidden-xs">
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li class="active">{{ $pageTitle }}</li>
                </ol>
            </div>
            <!-- /.col-xs-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</header>