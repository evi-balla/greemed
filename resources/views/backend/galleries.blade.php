@extends('backend.layouts.main')
@section('main_content')
<div class="row">
  <div class="col-xs-12 col-md-6">
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Create New Gallery</h3> </div>
      <!-- /.box-header -->
      <!-- form start -->
      <form role="form" name="galleryForm">
        <div class="box-body">
          <div class="form-group">
            <label for="name">Gallery Name</label>
            <input type="galleryId" class="form-control hidden" id="galleryId">
            <input type="galleryName" class="form-control" id="galleryName" placeholder="Enter gallery name">
          </div>
          <div class="form-group">
            <label for="images">Images:</label>
            <input type="file" name="images[]" id="images" multiple>
          </div>
          <div class="images-thumbs">
            <table class="table">
              <thead>
                <tr>
                  <td><b>Preview</b></td>
                  <td><b>Action</b></td>
                </tr>
              </thead>
              <tbody id="appendHere">

              </tbody>
            </table>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button id="btnCreateGallery" type="submit" class="btn btn-success">Create</button>
          <button id="btnUpdateGallery" type="submit" class="btn btn-warning hidden">Update</button>
          <button id="btnCancelUpdate" type="submit" class="btn btn-danger hidden">Cancel</button>
        </div>
      </form>
    </div>
  </div>
  <div class="col-xs-12 col-md-6">
    <div class="box box-dafault">
      <div class="box-header with-border">
        <h3 class="box-title">List of galleries</h3>
        <div class="box-tools pull-right">
          <div class="input-group" style="width: 150px;">
            <input type="text" name="table_search" id="table_search" class="form-control input-sm pull-right" placeholder="Search">
            <div class="input-group-btn">
              <button id="btnSearchGallery" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-responsive table-condensed table-hover" id="galleryTable">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody> </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer text-center">
        <div id="nav-pag"></div>
      </div>
      <!-- /.box-footer -->
    </div>
  </div>
    
  <style type="text/css">
    .pagination {
      margin: 0;
    }
  </style>
    
  <script>
  
    // send csrf token on every ajax request
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    
    /* global swal */
    
    // array of galleries to manipulate
    var galleries = [];
    var new_images = [];
    var new_images_thumbs = [];
    var old_images = [];
    
    // run
    getAllGalleries();
    
    // form create button
    $('#btnCreateGallery').click(function(event) {
      event.preventDefault();
      
      $('#overlay').removeClass('hidden');
      var fd = new FormData();
      fd.append('gallery_name', $('#galleryName').val());
      
      for(var i = 0; i < new_images.length; i++) {
          fd.append('images[]', new_images[i]);
      }
      
      $.ajax({
        url: '/admin/create-gallery',
        method: 'POST',
        processData: false,
        contentType: false,
        data: fd,
        success: function(res) {
          if (res.message == 'OK') {
            $('#overlay').addClass('hidden');
            galleries.unshift(res.result);
            new_images = [];
            new_images_thumbs = [];
            $('#appendHere').html = '';
            updateTable(galleries);
            $('#galleryName').val('');
            swal({
              title: 'Gallery created successfully',
              type: 'success'
            });
          }
          else {
            $('#overlay').addClass('hidden');
            swal({
              title: 'Error',
              text: res.result.gallery_name,
              type: 'error'
            });
          }
        }
      });
    });
    
    // action delete button
    $(document).on('click','#btnDeleteGallery', function(event) {
      
      event.preventDefault();
      
      var $gall_id = this.getAttribute('gallId');
      
      swal({
        title: "Are you sure?",
        text: "You will not be able to recover this gallery!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true
      }, function() {
        $('#overlay').removeClass('hidden');
        
        $.ajax({
          url: '/admin/delete-gallery',
          method: 'POST',
          data: {
            gallery_id: $gall_id
          },
          success: function(res) {
            if (res.message == 'OK') {
              $('#overlay').addClass('hidden');
              removeGalleryFromArray($gall_id);
              updateTable(galleries);
              swal({
                title: 'Gallery deleted successfully',
                type: 'success'
              });
            }
            else {
              $('#overlay').addClass('hidden');
              swal({
                title: 'Error',
                text: res.result.gallery_id,
                type: 'error'
              });
            }
          }
        });
      });
    });
    
    // action edit button
    $(document).on('click', '#btnEditGallery', function(event) {
      event.preventDefault();
      $('#overlay').removeClass('hidden');
      var $gall_id = this.getAttribute('gallId');
      
      $.ajax({
        url: '/admin/get-gallery-by-id',
        method: 'POST',
        data: {
          gallery_id: $gall_id
        },
        success: function(res) {
          if (res.message == 'OK') {
            galleries = [];
            old_images = [];
            new_images_thumbs = [];
            $('#appendHere').html('');
            addAndPreviewOldImages(res.result.images);
            $('#overlay').addClass('hidden');
            $('#galleryId').val(res.result.info.id);
            $('#galleryName').val(res.result.info.name);
            $('#btnCreateGallery').addClass('hidden');
            $('#btnCancelUpdate').removeClass('hidden');
            $('#btnUpdateGallery').removeClass('hidden');
          }
          else {
            $('#overlay').addClass('hidden');
            swal({
              title: 'Error',
              text: res.result.gallery_id,
              type: 'error'
            });
          }
        }
      });
    });
    
    // form update button
    $(document).on('click', '#btnUpdateGallery', function(event) {
      event.preventDefault();
      $('#overlay').removeClass('hidden');
      var $gall_id = $('#galleryId').val();
      var $gall_name = $('#galleryName').val();
      var fd = new FormData();
      
      if(new_images.length > 0 ) {
        for(var i = 0; i < new_images.length; i++) {
            fd.append('images[]', new_images[i]);
        }
        fd.append('gallery_id', $gall_id);
        fd.append('gallery_name', $gall_name);
        
        $.ajax({
          url: '/admin/update-gallery',
          method: 'POST',
          data: fd,
          contentType: false,
          processData: false,
          success: function(res) {
            if (res.message == 'OK') {
              updateGalleryInArray(res.result.id, res.result.name);
              updateTable(galleries);
              $('#overlay').addClass('hidden');
              $('#galleryId').val('');
              $('#galleryName').val('');
              $('#btnCreateGallery').removeClass('hidden');
              $('#btnCancelUpdate').addClass('hidden');
              $('#btnUpdateGallery').addClass('hidden');
            }
            else {
              $('#overlay').addClass('hidden');
              swal({
                title: 'Error',
                text: res.result.gallery_id,
                type: 'error'
              });
            }
          }
        });
      } else {
        $('#overlay').addClass('hidden');
        $('#galleryId').val('');
        $('#galleryName').val('');
        $('#btnCreateGallery').removeClass('hidden');
        $('#btnCancelUpdate').addClass('hidden');
        $('#btnUpdateGallery').addClass('hidden');
      }
    });
    
    // form cancel button 
    $(document).on('click', '#btnCancelUpdate', function(event) {
      event.preventDefault();
      $('.images-thumbs').html('');
      new_images = [];
      new_images_thumbs = [];
      $('#galleryId').val('');
      $('#galleryName').val('');
      $('#btnUpdateGallery').addClass('hidden');
      $('#btnCreateGallery').removeClass('hidden');
      $(this).addClass('hidden');
    });
    
    // search button
    $(document).on('click', '#btnSearchGallery', function(event) {
      event.preventDefault();
      $('#overlay').removeClass('hidden');
      
      var $gall_name = $('#table_search').val();
      
      $.ajax({
        url: '/admin/search-gallery-by-name',
        method: 'POST',
        data: {
          name: $gall_name
        },
        success: function(res) {
          if (res.message == 'OK') {
            $('#overlay').addClass('hidden');
            galleries = res.result;
            updateTable(galleries);
          }
          else {
            $('#overlay').addClass('hidden');
            swal({
              title: 'Error',
              text: JSON.stringify(res.result),
              type: 'error'
            });
          }
        }
      });
    });
    
    // append new elements to table
    function updateTable(data) {
      var tbody = $('#galleryTable tbody');
      emptyTable();
      $(data).each(function(row, element) {
        var tr = $('<tr>');
        var tdId = "<td id='" + element.id + "'>" + element.id + "</td>";
        var tdName = "<td id='" + element.name + "'>" + element.name + "</td>";
        var tdActions = "<td><div class='dropdown'>" + "<button class='btn btn-xs btn-primary dropdown-toggle' data-toggle='dropdown'>" + "Actions <span class='caret'></span>" + "</button>" + "<ul class='dropdown-menu'>" + "<li role='presentation'><a id='btnEditGallery' gallId='" + element.id + "' role='menuitem' tabindex='-1'><i class='fa fa-edit'></i> Edit</a></li>" + "<li role='presentation'><a id='btnDeleteGallery' gallId='" + element.id + "' role='menuitem' tabindex='-1'><i class='fa fa-remove'></i> Delete</a></li>" + "</ul>" + "</div></td>";
        tr.append(tdId, tdName, tdActions);
        tbody.append(tr);
      });
      pagginateTable();
    }
    
    // empty table
    function emptyTable(el) {
      if (typeof el != 'undefined') {
        el.innerHTML = "";
        return;
      }
      var tbody = $('#galleryTable tbody');
      tbody.empty();
      return;
    }
    
    // paginate table
    function pagginateTable() {
      $('#galleryTable').each(function() {
        var currentPage = 0;
            var numPerPage = 10;
            var $table = $(this);
            $table.bind('repaginate', function() {
                $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
            });
            $table.trigger('repaginate');
            var numRows = $table.find('tbody tr').length;
            var numPages = Math.ceil(numRows / numPerPage);
            var $pager = $('<ul class="pagination"></ul>');
            
            for (var page = 0; page < numPages; page++) {
                
                var $beginLi = '<li pageNr="' + (page) + '"class="page">'  ;
                var $anchor =  $beginLi + '<a href="#">'+ (page + 1) + '</a>';
                var $endLi = '</li>';
                var $line = $anchor + $endLi;
                $pager.append($line);
            } 
            	
            $( ".pagination" ).remove();
            $('#nav-pag').append($pager).find('li.page:first').addClass('active');
            
            $(".page").click(function() {
                    var newPage = $(this).attr('pagenr');
                    currentPage = newPage;
                    $table.trigger('repaginate');
                    $(this).addClass('active').siblings().removeClass('active');
            })
      });
    }
    
    // get all galleries 
    function getAllGalleries() {
      $('#overlay').removeClass('hidden');
      $.ajax({
        url: '/admin/get-all-galleries',
        method: 'POST',
        success: function(res) {
          if (res.message == 'OK') {
            $('#overlay').addClass('hidden');
            for(var c in res.result) {
              galleries.push(res.result[c]);
            }
            updateTable(galleries);
          }
          else {
            $('#overlay').addClass('hidden');
            swal({
              title: 'Error',
              text: res.result,
              type: 'error'
            });
          }
        }
      });
    }
    
    // get all files from input and push in array
    function addAndPreviewImages($fileContent) {
        var files = $fileContent;
        function readAndPreview(file) {
            if (/\.(jpe?g|png|gif)$/i.test(file.name)) {
                var reader = new FileReader();
                reader.addEventListener("load", function() {
                    var img = {
                        image: this.result,
                        name: file.name
                    };
                    new_images.push(file);
                    new_images_thumbs.push(img);

                     var to_append = $('<tr id="'+img.name+'"><td>'+
                    '<img width="100" src="'+img.image+'" name="'+img.name+'"></td>'+
                    '<td><button class="btn btn-danger" id="btnDeleteNewImage" name="'+img.name+'">Delete</button></td></tr>');
                    
                    $('#appendHere').append(to_append);
                    
                }, false);
                reader.readAsDataURL(file);
            }
        }
        
        if (files) {
            [].forEach.call(files, readAndPreview);
        }
        
    }
    
    $(document).on("click", "#btnDeleteNewImage", function(e) {
      debugger;
      e.preventDefault();
        var image_name = $("#btnDeleteNewImage").attr("name");
        deleteNewImage(image_name);
    })
    
    // delete new added image
    function deleteNewImage(image_name) {
      $('#' + image_name).html('');
      $.each(new_images, function(index, object) {
          if(object.name == parseInt(image_name)) {
              new_images.splice(index, 1);
              return false; // exit loop
          }
      })
    }
    
    // preview old images
    function addAndPreviewOldImages($fileContent) {
        var files = $fileContent;
        $.each(files, function(elem) {
          var img = {
            image: files[elem].image_path,
            id: files[elem].id
          };
          old_images.push(files[elem]);
          new_images_thumbs.push(img);
          
          var to_append = $('<tr image_id="' + img.id + '"><td>' +
            '<img width="100" src="' + img.image + '" name="' + img.id + '"></td>' +
            '<td><button class="btn btn-danger" id="btnDeleteOldImage" image_id="'+img.id+'">Delete</button></td></tr>');
          
          $('#appendHere').append(to_append);
        })
        
    }
    
    // handle on input images[] change
    $(document).on('change', '#images', function(event) {
        var files = this.files;
        addAndPreviewImages(files);
    });
    
    $(document).on('click', '#btnDeleteOldImage', function(event) {
        event.preventDefault();
        var image_id = $(this).attr('image_id');
        
        $.ajax({
          url: '/admin/delete-gallery-image-by-id',
          method: 'post',
          data: {image_id: image_id},
          success: function(res) {
            if(res.message == 'OK') {
              removeImageFromArray(null,image_id);
            } else {
              swal({
                title: 'ERROR',
                text: 'Could not delete image',
                type: 'error'
              });
            }
          }
        })
        
        
    })
    
    // remove gallery from array
    function removeGalleryFromArray(gallery_id)
    {
      $.each(galleries, function(index, object) {
          if(object.id == parseInt(gallery_id)) {
              galleries.splice(index, 1);
              return false; // exit loop
          }
      })
    }
    
    // get obj index in array
    function removeImageFromArray(name, id) {
      
      if(id) {
        $.each(old_images, function(index, object) {
          if(object.id == parseInt(id)) {
              $(document).find("[image_id='"+object.id+"']").remove();
              old_images.splice(index, 1);
              new_images_thumbs.splice(index, 1);
              return false; // exit loop
          }

        })
      } else {
        $.each(new_images, function(index, object) {
          if(object.name == name) {
              document.getElementById(name).remove();
              new_images.splice(index, 1);
              new_images_thumbs.splice(index, 1);
              return false; // exit loop
          }
        })
      }

    }
    
    // update object attribute in array
    function updateGalleryInArray(id, name) {
      $.each(galleries, function(index, object) {
          if(object.id == parseInt(id)) {
              object.name = name;
              return false; // exit loop
          }
      })
    }
    
    
  </script>
    
</div>

@stop