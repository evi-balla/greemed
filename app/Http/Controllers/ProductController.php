<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \App\Product as Product;
use \App\Category as Category;
use \App\SubCategory;
use DB;
use Exception;
use Validator;
use Input;
use Image;
use Config;
use Datatables;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class ProductController extends BaseController
{
    
    
    public function datatable()
    {
        $products = DB::table('products')->join('sub_categories', 'products.sub_category_id', '=', 'sub_categories.id')
            ->select(['products.id as id', 'products.name as name', 'products.description as description', 'products.description_en as description_en', 'products.image as image', 'products.created_at as created_at','sub_categories.id as sub_category_id', 'sub_categories.name as sub_category_name']);
        return Datatables::of($products)
            ->addColumn('action', function ($products) {
                return '<a id="btnEditProduct" product_id="' . $products->id . '" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <a id="btnDeleteProduct" product_id="' . $products->id . '" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete</a>';
            })
            ->make(true);
    }
    
    
    public function getProductById(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'id' => 'required|exists:products,id'
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 401);
            }
            
            $product = Product::find($request->input('id'));
            $sub_category_id = $product->subCategory;
            
            
            
            return $this->doResponse('OK', $product , 200);
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    public function store(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'sub_category_id' => 'required|exists:sub_categories,id',
                    'name' => 'required',
                    'description' => 'required',
                    'description_en' => 'required',
                    'image' => 'required'
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 401);
            }
            
            $product = new Product;
            $product->name = $request->input('name');
            $product->description = $request->input('description');
            $product->description_en = $request->input('description_en');
            
            
            if(Input::file('image')) {
                $image = Image::make(Input::file('image'));
                $image_name = $this->whash() . '.' . strtolower(Input::file('image')->getClientOriginalExtension());
                $image_destination = Config::get('app.public_path') . $this->productsStoragePath();
                $image_path = $image_destination . '/' . $image_name;
                $image->save($image_path);   
                
                $product->image = $this->productsStoragePath() . '/' . $image_name;
            }
            
            $product->sub_category_id = $request->input('sub_category_id');
            $product->save();
            
            return $this->doResponse('OK', 'Product added successfully', 200);
            
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }

    public function update(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'id' => 'required|exists:products,id',
                    'sub_category_id' => 'required|exists:sub_categories,id',
                    'name' => 'required',
                    'description' => 'required',
                    'description_en' => 'required',
                    'image' => 'required'
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 401);
            }
            
            $product = Product::find($request->input('id'));
            $product->name = $request->input('name');
            $product->description = $request->input('description');
            $product->description_en = $request->input('description_en');
            $product->sub_category_id = $request->input('sub_category_id');
            
            if(Input::exists('image')) {
                
                // delete old image
                if($product->image) {
                    unlink(Config::get('app.public_path') . $product->image);
                }
                
                $image = Image::make(Input::file('image'));
                $image_name = $this->whash() . '.' . strtolower(Input::file('image')->getClientOriginalExtension());
                $image_destination = Config::get('app.public_path') . $this->productsStoragePath();
                $image_path = $image_destination . '/' . $image_name;
                $image->save($image_path);   
                
                $product->image = $this->productsStoragePath() . '/' . $image_name;
            }
            
            $product->save();
            
            return $this->doResponse('OK', 'Product updated successfully', 200);
            
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }

    public function destroy(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'id'   => 'required|exists:products,id',
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 401);
            }
            
            $product = Product::find($request->input('id'));
            
            // delete old image
            if($product->image) {
                unlink(Config::get('app.public_path') . $product->image);
            }
            
            $product->delete();
            
            return $this->doResponse('OK', $product, 200);
            
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    
    public function getProductsByCategoryId(Request $request) {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'category_id'   => 'required|exists:categories,id',
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 401);
            }
            
            $page = $request->input('page');
            $page = !empty($page) ? $page : 1;
            $perPage = 10;
            
            $category = Category::find($request->input('category_id'));
            
            $products = $category->products;
            
            $result = new Paginator($products->forPage($page, $perPage), $products->count(), $perPage, $page);
            
            return $this->doResponse('OK', $result->toArray(), 200);
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    public function getProductsBySubCategoryId(Request $request) {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'sub_category_id'   => 'required|exists:sub_categories,id',
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 401);
            }
            
            $page = $request->input('page');
            $page = !empty($page) ? $page : 1;
            $perPage = 10;
            
            $sub_category = SubCategory::find($request->input('sub_category_id'));
            
            $products = $sub_category->products;
            
            $result = new Paginator($products->forPage($page, $perPage), $products->count(), $perPage, $page);
            
            return $this->doResponse('OK', $result->toArray(), 200);
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    public function searchProductByName(Request $request) {
        try {
            
            $string = $request->has('string') ? $request->input('string') : null;
            
            if(empty($string)) {
                 $products = Product::all();
                 return $this->doResponse('OK', $products, 200);
            }
            
            if (strlen($string) < 3) {
                return $this->doResponse('ERR', 'Search string must have at least 3 characters', 400);
            }
            
            $products = Product::where('name', 'like', '%'.$string.'%')->get();
            
            return $this->doResponse('OK', $products, 200);
            
        } catch (\Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
}
