<div class="container">
                        <section id="services" class="section">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <a href="" class="sbox">
                                        <div class="sbox-inner">
                                            <span class="sbox-icon"><i class="fa fa-laptop"></i></span>
                                            <h5 class="sbox-title">Fully Responsive Design</h5>
                                            <p>Donec in velit vel ipsum auctor pulvinar. Vesti bulum iaculis lacinia est. Proin dictum elementum velit. Fusce euismod auctor pulvinar.</p>
                                        </div>
                                    </a>
                                </div>
                                <!-- /.col-sm-4 -->
                                <div class="col-xs-12 col-sm-4">
                                    <a href="" class="sbox">
                                        <div class="sbox-inner">
                                            <span class="sbox-icon"><i class="fa fa-code"></i></span>
                                            <h5 class="sbox-title">Easy & Clean Code</h5>
                                            <p>Donec in velit vel ipsum auctor pulvinar. Vesti bulum iaculis lacinia est. Proin dictum elementum velit. Fusce euismod auctor pulvinar.</p>
                                        </div>
                                    </a>
                                </div>
                                <!-- /.col-sm-4 -->
                                <div class="col-xs-12 col-sm-4">
                                    <a href="" class="sbox">
                                        <div class="sbox-inner">
                                            <span class="sbox-icon"><i class="fa fa-paper-plane"></i></span>
                                            <h5 class="sbox-title">24/7 Customer Support</h5>
                                            <p>Donec in velit vel ipsum auctor pulvinar. Vesti bulum iaculis lacinia est. Proin dictum elementum velit. Fusce euismod auctor pulvinar.</p>
                                        </div>
                                    </a>
                                </div>
                                <!-- /.col-sm-4 -->
                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /#services -->
                        <section id="caroufredsel-portfolio" class="section type3">
                            <div class="row cnbox">
                                <div class="col-md-4">
                                    <div class="cbox">
                                        <h3 class="cbox-title">Recent Portfolio Items</h3>
                                        <span class="cbox-counter">12+</span>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="nbox">
                                        <p class="lead">Donec in velit vel ipsum auctor pulvinar. Vesti bulum iaculis lacinia est. Proin dictum elementum velit. Fusce euismod auctor pulvinar. Donec in velit vel ipsum auctor pulvinar.</p>
                                        <div class="controls">
                                            <span id="portfolio-prev" class="prev black"><i class="fa fa-angle-left"></i></span>
                                            <span id="portfolio-next" class="next black"><i class="fa fa-angle-right"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hide-overflow">
                                <div class="row">
                                    <div id="caroufredsel-portfolio-container">
                                        <div class="col-xs-12 col-sm-6 col-md-4 portfolio-item-wrapper artwork creative">
                                            <div class="portfolio-item type2">
                                                <div class="portfolio-thumb">
                                                    <img src="img/general/img1.jpg" class="img-responsive" alt="1st Portfolio Thumb">
                                                    <div class="image-overlay"></div>
                                                    <a href="img/general/img1.jpg" data-rel="prettyPhoto[pp_gal]" class="portfolio-zoom"><i class="fa fa-plus"></i></a>
                                                    <a href="portfolio-item-1.html" class="portfolio-link"><i class="fa fa-link"></i></a>
                                                </div>
                                                <div class="portfolio-details">
                                                    <div class="portfolio-meta">
                                                        <span class="portfolio-date"><i class="fa fa-clock-o"></i> <a href="#">&nbsp; 17 August, 2014</a></span>
                                                        <span class="portfolio-likes"><i class="fa fa-heart-o"></i> <a href="#">&nbsp; 12</a></span>
                                                    </div>
                                                    <h5 class="portfolio-title"><a href="">Has labitur disputando at, enim.</a></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.portfolio-item-wrapper -->
                                        <div class="col-xs-12 col-sm-6 col-md-4 portfolio-item-wrapper nature outside">
                                            <div class="portfolio-item type2">
                                                <div class="portfolio-thumb">
                                                    <img src="img/general/img2.jpg" class="img-responsive" alt="2nd Portfolio Thumb">
                                                    <div class="image-overlay"></div>
                                                    <a href="img/general/img2.jpg" data-rel="prettyPhoto[pp_gal]" class="portfolio-zoom"><i class="fa fa-plus"></i></a>
                                                    <a href="portfolio-item-1.html" class="portfolio-link"><i class="fa fa-link"></i></a>
                                                </div>
                                                <div class="portfolio-details">
                                                    <div class="portfolio-meta">
                                                        <span class="portfolio-date"><i class="fa fa-clock-o"></i> <a href="#">&nbsp; 17 August, 2014</a></span>
                                                        <span class="portfolio-likes"><i class="fa fa-heart-o"></i> <a href="#">&nbsp; 12</a></span>
                                                    </div>
                                                    <h5 class="portfolio-title"><a href="">Eu decore iisque sit.</a></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.portfolio-item-wrapper -->
                                        <div class="col-xs-12 col-sm-6 col-md-4 portfolio-item-wrapper photography artwork">
                                            <div class="portfolio-item type2">
                                                <div class="portfolio-thumb">
                                                    <img src="img/general/img3.jpg" class="img-responsive" alt="3rd Portfolio Thumb">
                                                    <div class="image-overlay"></div>
                                                    <a href="img/general/img3.jpg" data-rel="prettyPhoto[pp_gal]" class="portfolio-zoom"><i class="fa fa-plus"></i></a>
                                                    <a href="portfolio-item-1.html" class="portfolio-link"><i class="fa fa-link"></i></a>
                                                </div>
                                                <div class="portfolio-details">
                                                    <div class="portfolio-meta">
                                                        <span class="portfolio-date"><i class="fa fa-clock-o"></i> <a href="#">&nbsp; 17 August, 2014</a></span>
                                                        <span class="portfolio-likes"><i class="fa fa-heart-o"></i> <a href="#">&nbsp; 12</a></span>
                                                    </div>
                                                    <h5 class="portfolio-title"><a href="">Illum dolorum accommodare.</a></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.portfolio-item-wrapper -->
                                        <div class="col-xs-12 col-sm-6 col-md-4 portfolio-item-wrapper creative nature">
                                            <div class="portfolio-item type2">
                                                <div class="portfolio-thumb">
                                                    <img src="img/general/img4.jpg" class="img-responsive" alt="4th Portfolio Thumb">
                                                    <div class="image-overlay"></div>
                                                    <a href="img/general/img4.jpg" data-rel="prettyPhoto[pp_gal]" class="portfolio-zoom"><i class="fa fa-plus"></i></a>
                                                    <a href="portfolio-item-1.html" class="portfolio-link"><i class="fa fa-link"></i></a>
                                                </div>
                                                <div class="portfolio-details">
                                                    <div class="portfolio-meta">
                                                        <span class="portfolio-date"><i class="fa fa-clock-o"></i> <a href="#">&nbsp; 17 August, 2014</a></span>
                                                        <span class="portfolio-likes"><i class="fa fa-heart-o"></i> <a href="#">&nbsp; 12</a></span>
                                                    </div>
                                                    <h5 class="portfolio-title"><a href="">Eu aeque nusquam mea, ne est.</a></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.portfolio-item-wrapper -->
                                        <div class="col-xs-12 col-sm-6 col-md-4 portfolio-item-wrapper nature">
                                            <div class="portfolio-item type2">
                                                <div class="portfolio-thumb">
                                                    <img src="img/general/img5.jpg" class="img-responsive" alt="5th Portfolio Thumb">
                                                    <div class="image-overlay"></div>
                                                    <a href="img/general/img5.jpg" data-rel="prettyPhoto[pp_gal]" class="portfolio-zoom"><i class="fa fa-plus"></i></a>
                                                    <a href="portfolio-item-1.html" class="portfolio-link"><i class="fa fa-link"></i></a>
                                                </div>
                                                <div class="portfolio-details">
                                                    <div class="portfolio-meta">
                                                        <span class="portfolio-date"><i class="fa fa-clock-o"></i> <a href="#">&nbsp; 17 August, 2014</a></span>
                                                        <span class="portfolio-likes"><i class="fa fa-heart-o"></i> <a href="#">&nbsp; 12</a></span>
                                                    </div>
                                                    <h5 class="portfolio-title"><a href="">Mei no modo vitae mentitum.</a></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.portfolio-item-wrapper -->
                                    </div>
                                    <!-- /.caroufredsel-portfolio-container -->
                                </div>
                                <!-- /.row -->
                            </div>
                        </section>
                        <!-- /#caroufredsel-portfolio -->
                        <section id="action-box" class="section">
                            <div class="abox-wrap">
                                <div class="action-box">
                                    <h3>Hey, are you ready to buy this awesome theme?</h3>
                                    <p>Pellentesque pulvinar, risus dictum tempor luctus, quam augue blandit sapien.</p>
                                    <a href="#" class="btn adv-color squre">Purchase Today</a>
                                </div>
                            </div>
                            <!-- /.action-box -->
                        </section>
                        <!-- /#action-box -->
                        <section id="blog-posts" class="section type3">
                            <div class="row cnbox">
                                <div class="col-md-4">
                                    <div class="cbox">
                                        <h3 class="cbox-title">Latest Blog Posts</h3>
                                        <span class="cbox-counter">7+</span>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="nbox">
                                        <p class="lead">Donec in velit vel ipsum auctor pulvinar. Vesti bulum iaculis lacinia est. Proin dictum elementum velit. Fusce euismod auctor pulvinar.</p>
                                        <div class="controls">
                                            <span id="blog-posts-prev" class="prev"><i class="fa fa-angle-left"></i></span>
                                            <span id="blog-posts-next" class="next"><i class="fa fa-angle-right"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hide-overflow">
                                <div class="row">
                                    <div id="caroufredsel-blog-posts-container">
                                        <div class="col-xs-12 col-sm-6 col-md-4 post-wrapper">
                                            <article class="post type3">
                                                <div class="entry-thumb">
                                                    <img src="img/general/img8.jpg" class="img-responsive" alt="Responsive image">
                                                    <div class="image-overlay"></div>
                                                    <a href="img/general/img8.jpg" data-rel="prettyPhoto" class="ima-zoom"><i class="fa fa-plus"></i></a>
                                                </div>
                                                <!-- /.entry-thumb -->
                                                <div class="entry-wrap">
                                                    <div class="entry-meta">
                                                        <span class="entry-date"><i class="fa fa-clock-o"></i> <a href="#">12 August , 2014</a></span>
                                                        <span class="entry-comment pull-right"><i class="fa fa-comments"></i> <a href="">07</a></span>
                                                    </div>
                                                    <header class="entry-header">
                                                        <h5 class="entry-title"><a href="post-1.html">Eum ne adolescens disputando</a></h5>
                                                    </header>
                                                    <div class="entry-content">
                                                        <p>Donec in velit vel ipsum auctor pulvinar. Vesti bulum iaculis lacinia est. Proin dictum elementum velit.</p>
                                                    </div>
                                                    <!-- /.entry-content -->
                                                </div>
                                            </article>
                                        </div>
                                        <!-- /.col-md-4 -->
                                        <div class="col-xs-12 col-sm-6 col-md-4 post-wrapper">
                                            <article class="post type3">
                                                <div class="entry-thumb">
                                                    <!-- 16:9 aspect ratio -->
                                                    <div class="embed-responsive embed-responsive-10by8">
                                                        <iframe class="embed-responsive-item" src="//player.vimeo.com/video/30300114"></iframe>
                                                    </div>
                                                </div>
                                                <!-- /.entry-thumb -->
                                                <div class="entry-wrap">
                                                    <div class="entry-meta">
                                                        <span class="entry-date"><i class="fa fa-clock-o"></i> <a href="#">12 August , 2014</a></span>
                                                        <span class="entry-comment pull-right"><i class="fa fa-comments"></i> <a href="">07</a></span>
                                                    </div>
                                                    <header class="entry-header">
                                                        <h5 class="entry-title"><a href="post-1.html">Ex eros diceret vis sit an vide eruditi</a></h5>
                                                    </header>
                                                    <div class="entry-content">
                                                        <p>Donec in velit vel ipsum auctor pulvinar. Vesti bulum iaculis lacinia est. Proin dictum elementum velit.</p>
                                                    </div>
                                                    <!-- /.entry-content -->
                                                </div>
                                            </article>
                                        </div>
                                        <!-- /.col-md-4 -->
                                        <div class="col-xs-12 col-sm-6 col-md-4 post-wrapper">
                                            <article class="post type3">
                                                <div class="entry-thumb">
                                                    <div id="carousel-example-generic" class="carousel slide">
                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner">
                                                            <div class="item active">
                                                                <img src="img/general/img9.jpg" class="img-responsive" alt="Carousel Image">
                                                            </div>
                                                            <div class="item">
                                                                <img src="img/general/img10.jpg" class="img-responsive" alt="Carousel Image">
                                                            </div>
                                                            <div class="item">
                                                                <img src="img/general/img11.jpg" class="img-responsive" alt="Carousel Image">
                                                            </div>
                                                        </div>
                                                        <!-- Controls -->
                                                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                                        </a>
                                                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <!-- /.entry-thumb -->
                                                <div class="entry-wrap">
                                                    <div class="entry-meta">
                                                        <span class="entry-date"><i class="fa fa-clock-o"></i> <a href="#">12 August , 2014</a></span>
                                                        <span class="entry-comment pull-right"><i class="fa fa-comments"></i> <a href="">07</a></span>
                                                    </div>
                                                    <header class="entry-header">
                                                        <h5 class="entry-title"><a href="post-1.html">Mel nostro labitur efficiendi ad</a></h5>
                                                    </header>
                                                    <div class="entry-content">
                                                        <p>Donec in velit vel ipsum auctor pulvinar. Vesti bulum iaculis lacinia est. Proin dictum elementum velit.</p>
                                                    </div>
                                                    <!-- /.entry-content -->
                                                </div>
                                            </article>
                                            <!-- /.post -->
                                        </div>
                                        <!-- /.col-md-4 -->
                                        <div class="col-xs-12 col-sm-6 col-md-4 post-wrapper">
                                            <article class="post type3">
                                                <div class="entry-thumb">
                                                    <!-- 16:9 aspect ratio -->
                                                    <div class="embed-responsive embed-responsive-10by8">
                                                        <iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/130459845&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                                                    </div>
                                                </div>
                                                <!-- /.entry-thumb -->
                                                <div class="entry-wrap">
                                                    <div class="entry-meta">
                                                        <span class="entry-date"><i class="fa fa-clock-o"></i> <a href="#">12 August , 2014</a></span>
                                                        <span class="entry-comment pull-right"><i class="fa fa-comments"></i> <a href="">07</a></span>
                                                    </div>
                                                    <header class="entry-header">
                                                        <h5 class="entry-title"><a href="post-1.html">Eum facilisi consequat interesset id</a></h5>
                                                    </header>
                                                    <div class="entry-content">
                                                        <p>Donec in velit vel ipsum auctor pulvinar. Vesti bulum iaculis lacinia est. Proin dictum elementum velit.</p>
                                                    </div>
                                                    <!-- /.entry-content -->
                                                </div>
                                            </article>
                                        </div>
                                        <!-- /.col-md-4 -->
                                        <div class="col-xs-12 col-sm-6 col-md-4 post-wrapper">
                                            <article class="post type3">
                                                <div class="entry-thumb">
                                                    <img src="img/general/img12.jpg" class="img-responsive" alt="Responsive image">
                                                    <div class="image-overlay"></div>
                                                    <a href="img/general/img10.jpg" data-rel="prettyPhoto" class="ima-zoom"><i class="fa fa-plus"></i></a>
                                                </div>
                                                <!-- /.entry-thumb -->
                                                <div class="entry-wrap">
                                                    <div class="entry-meta">
                                                        <span class="entry-date"><i class="fa fa-clock-o"></i> <a href="#">12 August , 2014</a></span>
                                                        <span class="entry-comment pull-right"><i class="fa fa-comments"></i> <a href="">07</a></span>
                                                    </div>
                                                    <header class="entry-header">
                                                        <h5 class="entry-title"><a href="post-1.html">Per scaevola qualisque ne</a></h5>
                                                    </header>
                                                    <div class="entry-content">
                                                        <p>Donec in velit vel ipsum auctor pulvinar. Vesti bulum iaculis lacinia est. Proin dictum elementum velit.</p>
                                                    </div>
                                                    <!-- /.entry-content -->
                                                </div>
                                            </article>
                                        </div>
                                        <!-- /.col-md-4 -->
                                    </div>
                                    <!-- /#caroufredsel-blog-posts-container -->
                                </div>
                                <!-- /.row -->
                            </div>
                        </section>
                        <!-- /#blog-posts -->
                        <section id="our-clients" class="section type3">
                            <div class="row cnbox">
                                <div class="col-md-4">
                                    <div class="cbox">
                                        <h3 class="cbox-title">Our Clients</h3>
                                        <span class="cbox-counter">21+</span>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="nbox">
                                        <p class="lead">Here, as at Hong Kong and Calcutta, were mixed crowds of all races, Americans and English, Chinamen and Dutchmen, mostly merchants ready to buy or sell anything</p>
                                        <div class="controls">
                                            <span id="client-prev" class="prev"><i class="fa fa-angle-left"></i></span>
                                            <span id="client-next" class="next"><i class="fa fa-angle-right"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hide-overflow">
                                <div class="row">
                                    <div id="caroufredsel-clients-container">
                                        <div class="col-xs-12 col-sm-4 col-md-3 client-wrapper">
                                            <a href="#"><img class="img-responsive" src="img/clients/img5.jpg" alt="Client Image"></a>
                                        </div>
                                        <!-- /.client-wrapper -->
                                        <div class="col-xs-12 col-sm-4 col-md-3 client-wrapper">
                                            <a href="#"><img class="img-responsive" src="img/clients/img6.jpg" alt="Client Image"></a>
                                        </div>
                                        <!-- /.client-wrapper -->
                                        <div class="col-xs-12 col-sm-4 col-md-3 client-wrapper">
                                            <a href="#"><img class="img-responsive" src="img/clients/img1.jpg" alt="Client Image"></a>
                                        </div>
                                        <!-- /.client-wrapper -->
                                        <div class="col-xs-12 col-sm-4 col-md-3 client-wrapper">
                                            <a href="#"><img class="img-responsive" src="img/clients/img3.jpg" alt="Client Image"></a>
                                        </div>
                                        <!-- /.client-wrapper -->
                                        <div class="col-xs-12 col-sm-4 col-md-3 client-wrapper">
                                            <a href="#"><img class="img-responsive" src="img/clients/img2.jpg" alt="Client Image"></a>
                                        </div>
                                        <!-- /.client-wrapper -->
                                        <div class="col-xs-12 col-sm-4 col-md-3 client-wrapper">
                                            <a href="#"><img class="img-responsive" src="img/clients/img4.jpg" alt="Client Image"></a>
                                        </div>
                                        <!-- /.client-wrapper -->
                                    </div>
                                    <!-- /#caroufredsel-clients-container -->
                                </div>
                                <!-- /.row -->
                            </div>
                        </section>
                        <!-- /#our-clients -->
                    </div>