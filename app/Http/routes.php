<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => [ 'localeSessionRedirect', 'localizationRedirect' ]], function()
{
    
    Route::get('/auth/login', 'Auth\AuthController@getLogin');
    
    Route::get('/', 'PageController@homePage');
    Route::get('/about', 'PageController@aboutPage');
    Route::get('/contact', 'PageController@contactPage');
    Route::get('/quality', 'PageController@qualityPage');
    Route::get('/logistics', 'PageController@logisticsPage');
    Route::get('/news', 'PageController@newsPage');
    Route::get('/products', 'PageController@productsPage');
    Route::get('/gallery', 'PageController@galleryPage');
    Route::get('/product/{slug}', 'PageController@productPage');
    Route::get('/article/{slug}', 'PageController@articlePage');
    
    Route::get('/api/products/search', 'ProductController@searchProductByName');
    
    
});

Route::post('/auth/login', 'Auth\AuthController@postLogin');
Route::get('/auth/logout', 'Auth\AuthController@getLogout');
// Route::get('/auth/register', 'Auth\AuthController@getRegister');
// Route::post('/auth/register', 'Auth\AuthController@postRegister');

Route::post('/admin/get-products-by-category-id', 'ProductController@getProductsByCategoryId');
Route::post('/admin/get-products-by-sub-category-id', 'ProductController@getProductsBySubCategoryId');

Route::post('/api/sendMail', 'BaseController@sendMail');

Route::group(['middleware' => ['auth']], function()
{   
    /* Dashboard */
    Route::get('/admin', 'AdminController@index');
    Route::get('/admin/categories', 'AdminController@categories');
    Route::get('/admin/sub-categories', 'AdminController@subCategories');
    Route::get('/admin/articles', 'AdminController@articles');
    Route::get('/admin/employee-of-month', 'AdminController@employeeOfMonth');
    Route::get('/admin/products', 'AdminController@products');
    Route::get('/admin/galleries', 'AdminController@galleries');
    
    // api category
    Route::post('/admin/get-all-categories', 'CategoryController@getAllCategories');
    Route::post('/admin/create-category', 'CategoryController@store');
    Route::post('/admin/update-category', 'CategoryController@update');
    Route::post('/admin/delete-category', 'CategoryController@destroy');
    Route::post('/admin/get-category-by-id', 'CategoryController@getCategoryById');
    Route::post('/admin/search-category-by-name', 'CategoryController@searchCategoryByName');
    
    // api articles
    Route::post('/admin/get-all-articles', 'ArticleController@getAllArticles');
    Route::post('/admin/create-article', 'ArticleController@store');
    Route::post('/admin/update-article', 'ArticleController@update');
    Route::post('/admin/delete-article', 'ArticleController@destroy');
    Route::post('/admin/get-article-by-id', 'ArticleController@getArticleById');
    Route::post('/admin/search-article-by-title', 'ArticleController@searchArticleByTitle');
    
    // api sub category
    Route::post('/admin/get-all-sub-categories', 'SubCategoryController@getAllSubCategories');
    Route::post('/admin/create-sub-category', 'SubCategoryController@store');
    Route::post('/admin/update-sub-category', 'SubCategoryController@update');
    Route::post('/admin/delete-sub-category', 'SubCategoryController@destroy');
    Route::post('/admin/get-sub-category-by-id', 'SubCategoryController@getSubCategoryById');
    Route::post('/admin/search-sub-category-by-name', 'SubCategoryController@searchSubCategoryByName');
    
     // api gallery
    Route::post('/admin/get-all-galleries', 'GalleryController@getAllGalleries');
    Route::post('/admin/create-gallery', 'GalleryController@store');
    Route::post('/admin/update-gallery', 'GalleryController@update');
    Route::post('/admin/delete-gallery', 'GalleryController@destroy');
    Route::post('/admin/get-gallery-by-id', 'GalleryController@getGalleryById');
    Route::post('/admin/search-gallery-by-name', 'GalleryController@searchGalleryByName');
    Route::post('/admin/delete-gallery-image-by-id', 'GalleryController@deleteGalleryImageById');
    
    // api employee of month
    Route::post('admin/get-employee-of-month', 'EmployeeController@get');
    Route::post('admin/create-employee-of-month', 'EmployeeController@store');
    Route::post('admin/update-employee-of-month', 'EmployeeController@update');
    Route::post('admin/delete-employee-of-month', 'EmployeeController@destroy');
    
    // api products
    Route::get('admin/products-datatable', 'ProductController@datatable');
    Route::post('/admin/create-product', 'ProductController@store');
    Route::post('/admin/update-product', 'ProductController@update');
    Route::post('/admin/delete-product', 'ProductController@destroy');
    Route::post('/admin/get-product-by-id', 'ProductController@getProductById');
});