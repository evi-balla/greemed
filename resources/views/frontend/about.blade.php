@extends('frontend.layouts.main') 
@section('nav') 
@include('frontend.partials.nav') 
@stop 
@section('page_title')
@include('frontend.partials.page_title') 
@stop 
@section('main_container')

@if(LaravelLocalization::getCurrentLocale() == 'en')
<div class="row padding-bottom-50">
    <div class="col-xs-12">
        <p> <strong>Introduction</strong> </p>
        <br>
        <p> <strong>GREEMED S.A.</strong> (previously 2AC Pharma) was incorporated on 21 January 2009. The management team remained the same and continues forward with an even deeper experience level, strong integrity and a renewed commitment to quality.
            It became actively involved in business by mid 2009 focusing on the business of supplying medical and surgical equipment, instruments, models, and medical disposable items. Its main customers are Government hospitals-main and peripheries,
            dental centers, health centers, and private medical centers. </p>
        <br>
        <p> Progressively it has gained the attention and confident from both sectors as one of the many suppliers that seriously taking great care of the after sales service. As the horizon of the new millennium sets in, bringing with it the challenges and
            multitask of the business tactics and techniques, <strong>GREEMED S.A.</strong> shall strives to maximize and realize its objective as one of the respected and acknowledged supplier in the medical sector. </p>
        <br>
        <p><strong>Etymology</strong></p>
        <br>
        <p> The service name GREE comes from a hypothesis, Six De”gree”s of Separation postulated by social psychologist Stanley Milgram in 1967. Six degrees of separation is a hypothesis that everyone is approximately six steps away from any other person
            on Earth. If a chain of a friend of a friend statements are made, on average, any two people in the world can be connected in six steps or fewer. </p>
    </div>
</div>
@elseif(LaravelLocalization::getCurrentLocale() == 'it')
<div class="row padding-bottom-50">
    <div class="col-xs-12">
        <p> <strong>Introduzione</strong> </p>
        <br>
        <p> <strong>GREEMED S.A.</strong> (ex 2AC Pharma) è stata fondata  il 21 gennaio 2009. Il team di gestione è rimasto lo stesso e continua in progresso con un livello di esperienza crescente  negli anni, con una forte integrità e con un impegno  per rinnovare la qualità .E ‘coinvolto in attività dal inizio del 2009 concentrandosi sul business della fornitura di pressidi, apparecchi medicali e chirurgici,  ed articoli medici monouso. I suoi principali clienti sono gli ospedali stattali nella capital e nelle  periferie, i centri odontoiatrici, centri di salute e centri medici privati. </p>
        <br>
        <p> Progressivamente si è guadagnato l’attenzione e la fiducia da entrambi i settori (Publicho e Privato) come uno dei fornitori migliori che ofre un servizio nella cura del cliente anche nel post-vendita. L’arrivo dell nuovo millennio, ha apperto una nuova sfida per la societa che ha messo in atto delle nuove procedure  e techniche  per essere piu’ competitivi e al passo con tempo.
        <br>
        <strong>GREEMED S.A.</strong> e’ orientata nella massimizzazione e nella realizzazione del obiettivo per essere  uno dei fornitori piu’ riconosciuti nel settore medico sia quello Publicho e Privato, ofrendo un servizio di fornitura eficace e professionale.

 </p>
        <br>
        <p><strong>Etimologia</strong></p>
        <br>
        <p> Il  nome GREE dela societa’ deriva da un’ipotesi, “sei gradi”di separazione postulata dal psicologo sociale Stanley Milgram nel 1967. Sei gradi di separazione è un’ipotesi che ognuno è di circa sei passi di distanza da qualsiasi altra persona sulla Terra. </p>
    </div>
</div>
@else
<div class="row padding-bottom-50">
    <div class="col-xs-12">
        <p> <strong>Prezantim</strong> </p>
        <br>
        <p> <strong>Greenmed S.A</strong> ( e njohur më parëë si 2AC pharma) është themeluar ne 21 janar të vitit 2009.Ekipi  I  menxhimit mbetet po I njëjti dhe vazhdon përpara me një eksperiencë më të madhe, e fortë  dhe me një përkushtim të madh ndaj cilësi. Kompania është përfshirë aktivisht në biznes nga mesi I vitit 2009 me focus kryesor plotësimin me pajisje mjekësore dhe kirurgjikale, mjete, modele si edhe artikuj mjekësor një përdorimsh. </p>
        <br>
        <p> Kompania jonë vazhdimisht ka fituar vëmëndjen dhe besimin e dy sektorëve si një ndër  furnitorët  të cilët kujdesen për shërbimin pas shitjes. Ndërkohë që fillimi  I një mijëvjecari të ri  sjell  sfidat dhe përgjegjësitë  të taktiave dhe teknikave të biznesit GREEMED S.A do të përpiqet të maksimizojë dhe realizojë objektivat  e saj si një nga furnizuesit më të respektuar dhe më  të pranuar në sektorin e mjekësisë.
        </p>
        <br>
        <p><strong>Etimologia</strong></p>
        <br>
        <p>Emri I Kompanisë GREE e ka fillesën nga  teoria “ gjashtë fazat  e ndarjes” të faktuara  në vitin 1967 nga psikologu Stanley Milgram .Teoria e gjashtë fazave të ndarjes është në fakt  një hipotezë e cila shpjegon se çdo person është përafërsisht 6 hapa larg një personi tjeter.Nëse bëhet një deklatratë zinxhir  nga një mik referuar mikut të tij , mesatarisht çdo dy njerëz në botë  mund të  lidhen në gjashtë hapa ose më pak. </p>
    </div>
</div>
@endif

@stop 