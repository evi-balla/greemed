@extends('frontend.layouts.main') 
@section('nav') 
@include('frontend.partials.nav') 
@stop 
@section('page_title')
@include('frontend.partials.page_title') 
@stop 
@section('main_container')
<style>
    
    .product-image {
        background-size: cover !important;
        width: 100%;
        background-position: 50% 50% !important;
        height: 300px;
    }
    
</style>
<div id="main" class="wrapper">
    <div id="primary" class="site-comtent">
        <div class="container">
            <!-- /.portfolio-navigation -->
            
            <!-- /.portfolio-thumb -->
            <div class="row">
                <div class="col-xs-6">
                    <section class="portfolio-thumb section type2">
                        <div class="product-image" style="background: url('{{ $article->image }}');"></div>
                    </section>
                </div>
                <div class="col-xs-6">
                    <section class="section type2">
                        <h2>{{ $article->title }}</h2>
                        <br>
                        <!--<div class="subpage-title">-->
                        <!--    <h5>{{ Lang::get('translations.articles.article-description') }}</h5>-->
                        <!--</div>-->
                        {!! $article->description !!}
                    </section>
                </div>
            </div>
        </div>
    </div>
    <!-- /.site-comtent -->
</div>
@stop 