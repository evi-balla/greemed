<?php

    return [
    
        // NAV PAGES
        'menu.home' => 'Home',
        'menu.about' => 'Chi siamo',
        'menu.quality' => 'Qualita',
        'menu.products' => 'Prodotti',
        'menu.gallery' => 'Galleria',
        'menu.logistics' => 'Logistica',
        'menu.data_bank' => 'Banka dati',
        'menu.news' => 'Notizie',
        'menu.contact' => 'Contatto',
        'menu.language' => 'Lingua',
        
        
        // NAV PAGES
        'page.home' => 'Home',
        'page.about' => 'Chi siamo',
        'page.quality' => 'Qualita',
        'page.products' => 'Prodotti',
        'page.gallery' => 'Galleria',
        'page.logistics' => 'Logistica',
        'page.data_bank' => 'Banka dati',
        'page.news' => 'Notizie',
        'page.contact' => 'Contatto',
        
        // PAGES
        'home.newProducts' => 'Prodotti nuovi',
        'home.partners' => 'I NOSTRI COLLABORATORI',
        'home.news' => 'ULTIME NOTIZIE',
        'contact.visit-us' => 'VISITATECI',
        'contact.contact' => 'CONTATTO',
        'contact.input.name' => 'Nome*',
        'contact.input.email' => 'Email*',
        'contact.input.subject' => 'Soggetto',
        'contact.input.message' => 'Messaggio*',
        'contact.button.send' => 'Invia',
        'products.products-categories' => 'CATEGORIE',
        'products.product-description' => 'DESCRIZIONE DEL PRODOTTO',
        'products.product-details' => 'DETAGLI DEL PRODOTTO',
        'products.related-products' => 'PRODOTTI UGUALI',
        'products.product-details-category' => 'Categoria',
        'products.product-details-date' => 'Data',
        'articles.article-description' => 'DESCRIZIONE',
        
        // FOOTER
        'footer.contact' => 'RECAPITI',
        'footer.recent-products' => 'ULTIMI PRODOTTI',
        'footer.links' => 'LINKS',
        'footer.social' => 'SOCIAL',
        
    
    ]

?>