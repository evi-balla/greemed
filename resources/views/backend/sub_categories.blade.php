@extends('backend.layouts.main')
@section('main_content')
<div class="row">
  <div class="col-xs-12 col-md-6">
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Create new sub category</h3> </div>
      <!-- /.box-header -->
      <!-- form start -->
      <form role="form" name="categoryForm">
        <div class="box-body">
          <div class="form-group">
            <label for="name">Sub Category Name</label>
            <input type="text" class="form-control hidden" id="subCategoryId" name="subCategoryId">
            <input type="text" class="form-control" name="subCategoryName" id="subCategoryName" placeholder="Enter sub category name"> 
          </div>
            <select name="categoryId" id="categoryId" class="form-control chosen-select">
                <option value="" disabled selected>Select category...</option>
                @foreach($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button id="btnCreateSubCategory" type="submit" class="btn btn-success">Create</button>
          <button id="btnUpdateSubCategory" type="submit" class="btn btn-warning hidden">Update</button>
          <button id="btnCancelUpdate" type="submit" class="btn btn-danger hidden">Cancel</button>
        </div>
      </form>
    </div>
  </div>
  <div class="col-xs-12 col-md-6">
    <div class="box box-dafault">
      <div class="box-header with-border">
        <h3 class="box-title">List of sub categories</h3>
        <div class="box-tools pull-right">
          <div class="input-group" style="width: 150px;">
            <input type="text" name="table_search" id="table_search" class="form-control input-sm pull-right" placeholder="Search">
            <div class="input-group-btn">
              <button id="btnSearchSubCategory" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-responsive table-condensed table-hover" id="subCategoryTable">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Category Name</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody> </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer text-center">
        <div id="nav-pag"></div>
      </div>
      <!-- /.box-footer -->
    </div>
  </div>
    
  <style type="text/css">
    .pagination {
      margin: 0;
    }
  </style>
    
  <script>
  
    $(function(){
      run();
    })
    
    function run() {
      // send csrf token on every ajax request
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      
      // chosen select
      $("#categoryId").chosen({no_results_text: "Oops, nothing found!"});
      
      /* global swal */
      
      // array of categories to manipulate
      var sub_categories = [];
      
      // run
      getAllSubCategories();
      
      // form create button
      $('#btnCreateSubCategory').click(function(event) {
        event.preventDefault();
        
        $('#overlay').removeClass('hidden');
        
        $.ajax({
          url: '/admin/create-sub-category',
          method: 'POST',
          data: {
            sub_category_name: $('#subCategoryName').val(),
            category_id: $('#categoryId').val()
          },
          success: function(res) {
            if (res.message == 'OK') {
              sub_categories.unshift(res.result);
              updateTable(sub_categories);
              $('#subCategoryName').val('');
              $('#categoryId').val('');
              $('#categoryId').trigger('chosen:updated');
              swal({
                title: 'Sub Category created successfully',
                type: 'success'
              });
            }
            else {
              swal({
                title: 'Error',
                text: res.result.sub_category_name ? res.result.sub_category_name : (res.result.category_id ? res.result.category_id : 'APP ERROR. CONTACT DEVELOPER'),
                type: 'error'
              });
            }
            $('#overlay').addClass('hidden');
          }
        });
      });
      
      // action delete button
      $(document).on('click','#btnDeleteSubCategory', function(event) {
        
        event.preventDefault();
        
        var $sub_cat_id = this.getAttribute('subCatId');
        
        swal({
          title: "Are you sure?",
          text: "You will not be able to recover this sub category!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: true
        }, function() {
          $('#overlay').removeClass('hidden');
          
          $.ajax({
            url: '/admin/delete-sub-category',
            method: 'POST',
            data: {
              sub_category_id: $sub_cat_id
            },
            success: function(res) {
              if (res.message == 'OK') {
                removeSubCategoryFromArray($sub_cat_id);
                updateTable(sub_categories);
                swal({
                  title: 'Category deleted successfully',
                  type: 'success'
                });
              }
              else {
                swal({
                  title: 'Error',
                  text: res.result.category_id,
                  type: 'error'
                });
              }
              $('#overlay').addClass('hidden');
            }
          });
        });
      });
      
      // action edit button
      $(document).on('click', '#btnEditSubCategory', function(event) {
        event.preventDefault();
        $('#overlay').removeClass('hidden');
        var $sub_cat_id = this.getAttribute('subCatId');
        
        $.ajax({
          url: '/admin/get-sub-category-by-id',
          method: 'POST',
          data: {
            sub_category_id: $sub_cat_id
          },
          success: function(res) {
            if (res.message == 'OK') {
              $('#subCategoryId').val(res.result.id);
              $('#subCategoryName').val(res.result.name);
              $('#categoryId').val(res.result.category_id);
              $('#categoryId').trigger('chosen:updated');
              $('#btnCreateSubCategory').addClass('hidden');
              $('#btnCancelUpdate').removeClass('hidden');
              $('#btnUpdateSubCategory').removeClass('hidden');
            }
            else {
              $('#overlay').addClass('hidden');
              swal({
                title: 'Error',
                text: res.result.id,
                type: 'error'
              });
            }
            $('#overlay').addClass('hidden');
          }
        });
      });
      
      // form update button
      $(document).on('click', '#btnUpdateSubCategory', function(event) {
        event.preventDefault();
        $('#overlay').removeClass('hidden');
        
        var $sub_cat_id = $('#subCategoryId').val();
        var $cat_id = $('#categoryId').val();
        var $sub_cat_name = $('#subCategoryName').val();
        
        $.ajax({
          url: '/admin/update-sub-category',
          method: 'POST',
          data: {
            sub_category_id: $sub_cat_id,
            category_id: $cat_id,
            sub_category_name: $sub_cat_name
          },
          success: function(res) {
            if (res.message == 'OK') {
              updateSubCategoryInArray(res.result.id, res.result.name, res.result.category_name);
              updateTable(sub_categories);
              $('#subCategoryId').val('');
              $('#categoryId').val('');
              $('#categoryId').trigger('chosen:updated');
              $('#subCategoryName').val('');
              $('#btnCreateSubCategory').removeClass('hidden');
              $('#btnCancelUpdate').addClass('hidden');
              $('#btnUpdateSubCategory').addClass('hidden');
            }
            else {
              swal({
                title: 'Error',
                text: res.result.sub_category_name ? res.result.sub_category_name : (res.result.category_id ? res.result.category_id : 'APP ERROR. CONTACT DEVELOPER'),
                type: 'error'
              });
            }
            $('#overlay').addClass('hidden');
          }
        });
      });
      
      // form cancel button 
      $(document).on('click', '#btnCancelUpdate', function(event) {
        event.preventDefault();
        $('#subCategoryId').val('');
        $('#subCategoryName').val('');
        $('#categoryId').val("");
        $('#categoryId').trigger('chosen:updated');
        $('#btnUpdateSubCategory').addClass('hidden');
        $('#btnCreateSubCategory').removeClass('hidden');
        $(this).addClass('hidden');
      });
      
      // search button
      $(document).on('click', '#btnSearchSubCategory', function(event) {
        event.preventDefault();
        $('#overlay').removeClass('hidden');
        
        var $sub_cat_name = $('#table_search').val();
        
        $.ajax({
          url: '/admin/search-sub-category-by-name',
          method: 'POST',
          data: {
            name: $sub_cat_name
          },
          success: function(res) {
            if (res.message == 'OK') {
              $('#overlay').addClass('hidden');
              sub_categories = res.result;
              updateTable(sub_categories);
            }
            else {
              $('#overlay').addClass('hidden');
              swal({
                title: 'Error',
                text: JSON.stringify(res.result),
                type: 'error'
              });
            }
          }
        });
      });
      
      // append new elements to table
      function updateTable(data) {
        var tbody = $('#subCategoryTable tbody');
        emptyTable();
        $(data).each(function(row, element) {
          var tr = $('<tr>');
          var tdId = "<td id='" + element.id + "'>" + element.id + "</td>";
          var tdName = "<td id='" + element.name + "'>" + element.name + "</td>";
          var tdCatName = "<td id='" + element.category_name + "'>" + element.category_name + "</td>";
          var tdActions = "<td><div class='dropdown'>" + "<button class='btn btn-xs btn-primary dropdown-toggle' data-toggle='dropdown'>" + "Actions <span class='caret'></span>" + "</button>" + "<ul class='dropdown-menu'>" + "<li role='presentation'><a id='btnEditSubCategory' subCatId='" + element.id + "' role='menuitem' tabindex='-1'><i class='fa fa-edit'></i> Edit</a></li>" + "<li role='presentation'><a id='btnDeleteSubCategory' subCatId='" + element.id + "' role='menuitem' tabindex='-1'><i class='fa fa-remove'></i> Delete</a></li>" + "</ul>" + "</div></td>";
          tr.append(tdId, tdName, tdCatName, tdActions);
          tbody.append(tr);
        });
        pagginateTable();
      }
      
      // empty table
      function emptyTable(el) {
        if (typeof el != 'undefined') {
          el.innerHTML = "";
          return;
        }
        var tbody = $('#subCategoryTable tbody');
        tbody.empty();
        return;
      }
      
      // paginate table
      function pagginateTable() {
        $('#subCategoryTable').each(function() {
          var currentPage = 0;
              var numPerPage = 10;
              var $table = $(this);
              $table.bind('repaginate', function() {
                  $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
              });
              $table.trigger('repaginate');
              var numRows = $table.find('tbody tr').length;
              var numPages = Math.ceil(numRows / numPerPage);
              var $pager = $('<ul class="pagination"></ul>');
              
              for (var page = 0; page < numPages; page++) {
                  
                  var $beginLi = '<li pageNr="' + (page) + '"class="page">'  ;
                  var $anchor =  $beginLi + '<a href="#">'+ (page + 1) + '</a>';
                  var $endLi = '</li>';
                  var $line = $anchor + $endLi;
                  $pager.append($line);
              } 
              	
              $( ".pagination" ).remove();
              $('#nav-pag').append($pager).find('li.page:first').addClass('active');
              
              $(".page").click(function() {
                      var newPage = $(this).attr('pagenr');
                      currentPage = newPage;
                      $table.trigger('repaginate');
                      $(this).addClass('active').siblings().removeClass('active');
              })
        });
      }
      
      // get all categories 
      function getAllSubCategories() {
        $('#overlay').removeClass('hidden');
        $.ajax({
          url: '/admin/get-all-sub-categories',
          method: 'POST',
          success: function(res) {
            if (res.message == 'OK') {
              $('#overlay').addClass('hidden');
              for(var sc in res.result) {
                sub_categories.push(res.result[sc]);
              }
              updateTable(sub_categories);
            }
            else {
              swal({
                title: 'Error',
                text: res.result,
                type: 'error'
              });
            }
          }
        });
      }
      
      // get obj index in array and remove it
      function removeSubCategoryFromArray(id) {
        $.each(sub_categories, function(index, object) {
            if(object.id == parseInt(id)) {
                sub_categories.splice(index, 1);
                return false; // exit loop
            }
        })
      }
      
      // update object attribute in array
      function updateSubCategoryInArray(id, name, category_name) {
        $.each(sub_categories, function(index, object) {
            if(object.id == parseInt(id)) {
                object.name = name;
                object.category_name = category_name;
                return false; // exit loop
            }
        })
      }
    }

  </script>
    
</div>
@stop