<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \App\Category as Category;
use Exception;
use Validator;

class CategoryController extends BaseController
{

    public function index()
    {
        try {
            
            $categories = Category::all();
            
            return $this->doResponse('OK', $categories, 200);

            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }

    public function store(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'category_name' => 'required'
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 401);
            }
            
            $category = new Category;
            $category->name = $request->input('category_name');
            $category->save();
            
            return $this->doResponse('OK', $category, 200);
            
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }

    public function update(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'category_id'   => 'required|exists:categories,id',
                    'category_name' => 'required'
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 401);
            }
            
            $category = Category::find($request->input('category_id'));
            $category->name = $request->input('category_name');
            $category->save();
            
            return $this->doResponse('OK', $category, 200);
            
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }

    public function destroy(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'category_id'   => 'required|exists:categories,id',
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 401);
            }
            
            $category = Category::find($request->input('category_id'));
            $category->delete();
            
            return $this->doResponse('OK', $category, 200);
            
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    public function getAllCategories()
    {
        try {
            
            $categories = Category::all();
            
            return $this->doResponse('OK', $categories , 200);
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    public function getCategoryById(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'category_id' => 'required|exists:categories,id'
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 401);
            }
            
            $category = Category::find($request->input('category_id'));
            
            return $this->doResponse('OK', $category , 200);
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    public function searchCategoryByName(Request $request)
    {
        try {
            
            if(strlen($request->input('name')) > 0 ) {
                $res = Category::where('name', 'like', '%'.$request->input('name').'%')->get();
            } else {
                $res = Category::all();
            }
            
            return $this->doResponse('OK', $res , 200);
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
}
