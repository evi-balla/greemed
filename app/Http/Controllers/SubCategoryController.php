<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \App\SubCategory as SubCategory;
use \App\Category as Category;
use Exception;
use Validator;
use Input;
use DB;

class SubCategoryController extends BaseController
{
    
   
    public function store(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'category_id' => 'required|exists:categories,id',
                    'sub_category_name' => 'required'
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 401);
            }
            
            $sub_category = new SubCategory;
            $sub_category->name = $request->input('sub_category_name');
            $sub_category->category_id = $request->input('category_id');
            $sub_category->save();
            
            $res = DB::table('sub_categories')
                            ->join('categories', 'sub_categories.category_id', '=', 'categories.id')
                            ->select('sub_categories.id as id', 'sub_categories.name as name', 'categories.id as category_id', 'categories.name as category_name')
                            ->where('sub_categories.id', '=', $sub_category->id)
                            ->first();
            
            return $this->doResponse('OK', $res, 200);
            
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    public function update(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'sub_category_id'   => 'required|exists:sub_categories,id',
                    'sub_category_name' => 'required',
                    'category_id' => 'required|exists:categories,id'
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 401);
            }
            
            $sub_category = SubCategory::find($request->input('sub_category_id'));
            
            if(Input::exists('sub_category_name')) {
                $sub_category->name = $request->input('sub_category_name');
            }
            
            if(Input::exists('category_id')) {
                $sub_category->category_id = $request->input('category_id');
            }
            
            $sub_category->save();
            
            $res = DB::table('sub_categories')
                            ->join('categories', 'sub_categories.category_id', '=', 'categories.id')
                            ->select('sub_categories.id as id', 'sub_categories.name as name', 'categories.id as category_id', 'categories.name as category_name')
                            ->where('sub_categories.id', '=', $sub_category->id)
                            ->first();
            
            return $this->doResponse('OK', $res, 200);
            
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    public function destroy(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'sub_category_id'   => 'required|exists:sub_categories,id',
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 401);
            }
            
            $sub_category = SubCategory::find($request->input('sub_category_id'));
            $sub_category->delete();
            
            return $this->doResponse('OK', $sub_category, 200);
            
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    public function getAllSubCategories()
    {
        try {
            
            $sub_categories = DB::table('sub_categories')
                            ->join('categories', 'sub_categories.category_id', '=', 'categories.id')
                            ->select('sub_categories.id as id', 'sub_categories.name as name', 'categories.id as category_id', 'categories.name as category_name')
                            ->get();

            
            return $this->doResponse('OK', $sub_categories , 200);
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    public function getByCategory(Request $request)
    {
        try {
            
             $validator = Validator::make(
                $request->all(),
                [
                    'category_id'   => 'required|exists:categories,id',
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 401);
            }
            
            $category = Category::find($request->input('id'));
            
            $sub_categories = $category->subCategories;
            
            return $this->doResponse('OK', $sub_categories, 200);
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    public function getSubCategoryById(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'sub_category_id' => 'required|exists:sub_categories,id'
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 401);
            }
            
            $sub_category = SubCategory::find($request->input('sub_category_id'));
            
            $res = DB::table('sub_categories')
                            ->join('categories', 'sub_categories.category_id', '=', 'categories.id')
                            ->select('sub_categories.id as id', 'sub_categories.name as name', 'categories.id as category_id', 'categories.name as category_name')
                            ->where('sub_categories.id', '=', $sub_category->id)
                            ->first();
            
            return $this->doResponse('OK', $res , 200);
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    public function searchSubCategoryByName(Request $request)
    {
        try {
            
           
            
            $res = [];
            
            if(strlen($request->input('name')) > 0 ) {
                $res = DB::table('sub_categories')
                    ->join('categories', 'sub_categories.category_id', '=', 'categories.id')
                    ->select('sub_categories.id as id', 'sub_categories.name as name', 'categories.id as category_id', 'categories.name as category_name')
                    ->where('sub_categories.name', 'like', '%'.$request->input('name').'%')
                    ->get();
            } else {
                $res = DB::table('sub_categories')
                    ->join('categories', 'sub_categories.category_id', '=', 'categories.id')
                    ->select('sub_categories.id as id', 'sub_categories.name as name', 'categories.id as category_id', 'categories.name as category_name')
                    ->first();
            }
            
            return $this->doResponse('OK', $res , 200);
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    
}
