<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Mail;

class BaseController extends Controller
{
    public function doResponse($message, $result, $status)
    {
        $data = [
            'message'   => $message,
            'result'    => $result,
            'status'    => $status
        ];
        return response()->json($data);
        die();
    }
    
    
    public function productsStoragePath()
    {
        return '/storage/products';
    }
    
    public function articlesStoragePath()
    {
        return '/storage/articles';
    }
    
    public function galleriesStoragePath()
    {
        return '/storage/galleries';
    }
    
    public function wHash(){
        return hash_hmac('sha256', str_random(20), time());
    }
    
    public function mailSender($template, $data, $to, $from, $subject) {
        $sent = Mail::send($template, $data, function($message) use($to, $from, $subject) {
            $message->to($to);
            $message->from($from);
            $message->subject($subject);
        });
        return $sent;
    }
    
    public function sendMail()
    {
        try {
            
            $name = Input::get('name');
            $email = Input::get('email');
            $subject = Input::get('subject');
            $message = Input::get('message');
            
            $data = [
                'email' => $email,
                'subject' => $subject,
                'message_body' => $message
            ];
            
            $is_sent = $this->mailSender('email.contact', $data, env('MAIL_USERNAME'), $email, 'Contact from web - '. $subject);
            if($is_sent == true) {
                return $this->doResponse('OK', 'Email was sent successfully', 200);
            } else {
                return $this->doResponse('ERR', 'Error sending email', 400);
            }
            
        } catch (Exception $e) {
            return response()->json(['status' => 'error', 'message'=>$e->getMessage()]);
        }
    }
    
}
