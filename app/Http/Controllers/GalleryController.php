<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \App\Gallery as Gallery;
use \App\GalleryImages as GalleryImages;
use Input;
use Image;
use Config;
use Exception;
use Validator;

class GalleryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            
            $galleries = Gallery::all();
            
            return $this->doResponse('OK', $galleries, 200);

            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    
    public function store(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'gallery_name' => 'required'
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 400);
            }
            
            $gallery = new Gallery;
            $gallery->name = $request->input('gallery_name');
            $gallery->save();
            
            
            // upload images
            
            if(Input::get('images') != 'undefined') {
                
                $images = Input::file('images');
                
                foreach($images as $image) {
                    
                    $gallery_image = new GalleryImages;
                    
                    $img = Image::make($image);
                    $image_name = $this->whash() . '.' . strtolower($image->getClientOriginalExtension());
                    $image_destination = Config::get('app.public_path') . $this->galleriesStoragePath();
                    $image_path = $image_destination . '/' . $image_name;
                    $img->save($image_path);   
                    
                    $gallery_image->image_path = $this->galleriesStoragePath() . '/' . $image_name;
                    $gallery_image->gallery_id = $gallery->id;
                    $gallery_image->save();
                    
                }
            }
            
            
            return $this->doResponse('OK', $gallery, 200);
            
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }

public function update(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'gallery_id'   => 'required|exists:galleries,id',
                    'gallery_name' => 'required'
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 401);
            }
            
            $gallery = Gallery::find($request->input('gallery_id'));
            $gallery->name = $request->input('gallery_name');
            $gallery->save();
            
            // update images
            if(Input::get('images') != 'undefined') {
                
                $images = Input::file('images');
                
                // delete old images
                $gallery_images = $gallery->images;
                
                foreach($gallery_images as $gi) {
                    unlink(Config::get('app.public_path') . $gi->image_path);
                    $gi->delete();
                }
                
                // insert new images
                foreach($images as $image) {
                    
                    $gallery_image = new GalleryImages;
                    
                    $img = Image::make($image);
                    $image_name = $this->whash() . '.' . strtolower($image->getClientOriginalExtension());
                    $image_destination = Config::get('app.public_path') . $this->galleriesStoragePath();
                    $image_path = $image_destination . '/' . $image_name;
                    $img->save($image_path);   
                    
                    $gallery_image->image_path = $this->galleriesStoragePath() . '/' . $image_name;
                    $gallery_image->gallery_id = $gallery->id;
                    $gallery_image->save();
                    
                }
            }
            
            
            return $this->doResponse('OK', $gallery, 200);
            
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
     public function destroy(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'gallery_id'   => 'required|exists:galleries,id',
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 401);
            }
            
            $gallery = Gallery::find($request->input('gallery_id'));
            $gallery->delete();
            
            return $this->doResponse('OK', $gallery, 200);
            
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
        }
    
    
    public function getAllGalleries()
    {
        try {
            
            $galleries = Gallery::all();
            
            return $this->doResponse('OK', $galleries , 200);
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    public function getGalleryById(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'gallery_id' => 'required|exists:galleries,id'
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 401);
            }
            
            $data = [
                'info',
                'images'
            ];
            
            $gallery = Gallery::find($request->input('gallery_id'));
            $images = $gallery->images;
            
            $data['info'] = $gallery;
            $data['images'] = $images;
            
            return $this->doResponse('OK', $data , 200);
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    public function searchGalleryByName(Request $request)
    {
        try {
            
            if(strlen($request->input('name')) > 0 ) {
                $res = Gallery::where('name', 'like', '%'.$request->input('name').'%')->get();
            } else {
                $res = Gallery::all();
            }
            
            return $this->doResponse('OK', $res , 200);
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    public function deleteGalleryImageById(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'image_id' => 'required'
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 400);
            }
            
            $image = GalleryImages::find($request->input('image_id'));
            
            unlink(Config::get('app.public_path') . $image->image_path);
            $image->delete();
            
            return $this->doResponse('OK', $image, 200);
            
        } catch (Exception $e) {
             return $this->doResponse('ERR', $e->getMessage(), 500);
        }
        
        
    }
    
    
}