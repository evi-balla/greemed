<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GalleryImages extends Model
{
    protected $table = 'gallery_images';
    
    protected $fillable = ['image_path', 'gallery_id'];
    
    public function gallery() {
        return $this->belongsTo('App\Gallery');
    }
}
