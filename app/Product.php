<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Product extends Model implements SluggableInterface
{
    protected $table = 'products';
    
    protected $fillable = ['name', 'description', 'description_en', 'image', 'sub_category_id'];
    
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
    ];
    
    public function subCategory(){
        return $this->belongsTo('App\SubCategory', 'sub_category_id', 'id');
    }
    
}
