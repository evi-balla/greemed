<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Category extends Model implements SluggableInterface
{
    protected $table = 'categories';
    
    protected $fillable = ['name', 'slug'];
    
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
    ];
    
    public function subCategories() {
        return $this->hasMany('App\SubCategory');
    }
    
    public function products() {
        return $this->hasManyThrough('App\Product', 'App\SubCategory', 'category_id', 'sub_category_id');
    }
    
}
