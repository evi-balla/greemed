@extends('backend.layouts.main')
@section('main_content')
<div class="row">
  <!-- modal form -->
  
<div class="modal fade" id="productModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal Default</h4> </div>
      <div class="modal-body">
        <form id="productsForm">
          <input type="hidden" name="productId" id="productId"/>
          <div class="form-group">
            <label for="name">Name</label>
            <div class="text-center">
              <input type="text" name="name" class="form-control" id="name" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <label for="sub_category_id">Sub Category Name</label>
            <select name="sub_category_id" id="sub_category_id" class="form-control chosen-select">
              <option>Select sub category</option>
              @foreach($sub_categories as $sc)
              <option value="{{ $sc->id }}">{{ $sc->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="description">Description</label>
            <textarea rows="4" name="description" id="description" class="form-control">
            </textarea>
          </div>
          <div class="form-group">
            <label for="description">Description English</label>
            <textarea rows="4" name="description_en" id="description_en" class="form-control">
            </textarea>
          </div>
          <div class="form-group">
            <label for="name">Image</label>
            <div class="text-center">
              <input type="file" name="image" accept="image/*" class="form-control" id="image" class="form-control" required>
              <img src="" class="hidden" id="image_thumb"></img>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary hidden" id="btnSaveProduct" >Save changes</button>
        <button type="button" class="btn btn-primary" id="btnStoreProduct" >Save</button>
      </div>
    </div>
  </div>
</div>

  <!-- datatable  -->
  <div class="col-xs-12">
    <div class="box box-dafault">
      <div class="box-header with-border">
        <h3 class="box-title">List of products</h3>
        <div class="box-tools pull-right">
            <div class="form-group">
                <a id="btnNewProduct" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-plus"></i> Add New</a>
            </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-responsive table-condensed table-hover" id="productsTable">
          <thead>
            <tr>
              <th>ID</th>
              <th>Title</th>
              <th>Sub Category</th>
              <th>Created At</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody> </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer text-center">
        <div id="nav-pag"></div>
      </div>
      <!-- /.box-footer -->
    </div>
  </div>
    
  <style type="text/css">
    .pagination {
        margin: 0;
    }
    #image_thumb {
        max-height: 100px;
        margin: 10px;
        padding: 5px;
        border: solid 1px #ddd;
    }
  </style>
    
</div>
<script>
  
    // send csrf token on every ajax request
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    
    /* global swal, CKEDITOR */
    
    $(document).ready(function() {
      
      
      CKEDITOR.replace('description');
      CKEDITOR.replace('description_en');

      var dt;
      // inject chosen plugin for input
      $('#sub_category_id').chosen({
        no_results_text: "Oops, nothing found!",
        width: "100%",
        max_shown_results: 20
      });
        
      // datatable
      datatable();
      
      function datatable() {
         dt =  $('#productsTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/admin/products-datatable',
            columns: [
                {data: 'id', name: 'products.id'},
                {data: 'name', name: 'products.name'},
                {data: 'sub_category_name', name: 'sub_categories.name'},
                {data: 'created_at', name: 'products.created_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
      }
      
          // form create button
    $('#btnNewProduct').click(function(event) {
      event.preventDefault();
      cleanForm();
      $('.modal-title').html('Add new product');
      $('#btnStoreProduct').removeClass('hidden');
      $('#btnSaveProduct').addClass('hidden');
      $('#productModal').modal({
          backdrop: 'static',
          keyboard: false
      });
  
    });
    
    $(document).on('click', '#btnStoreProduct', function(event) {
      event.preventDefault();
      $('#overlay').removeClass('hidden');
      var fd = new FormData();
      fd.append('name', $('#name').val());
      fd.append('description', CKEDITOR.instances.description.getData());
      fd.append('description_en', CKEDITOR.instances.description_en.getData());
      fd.append('sub_category_id', $('#sub_category_id').val());
      fd.append('image', document.getElementById('image').files[0]);
      
      $.ajax({
        url: '/admin/create-product',
        method: 'POST',
        processData: false,
        contentType: false,
        data: fd,
        success: function(res) {
          if (res.message == 'OK') {
            dt.ajax.reload();
            cleanForm();
            $('#overlay').addClass('hidden');
            $('#productModal').modal('hide');
            swal({
              title: 'Product added successfully',
              type: 'success'
            });
          }
          else {
            $('#overlay').addClass('hidden');
            swal({
              title: 'Error',
              text: res.result.name ? res.result.name : res.result.description ? res.result.description : res.result.description_en ? res.result.description_en : res.result.sub_category_id ? res.result.sub_category_id : res.result,
              type: 'error'
            });
          }
        }
      });
    });
    
    // action delete button
    $(document).on('click','#btnDeleteProduct', function(event) {
      
      event.preventDefault();
      
      var $product_id = this.getAttribute('product_id');
      
      swal({
        title: "Are you sure?",
        text: "You will not be able to recover this product!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true
      }, function() {
        $('#overlay').removeClass('hidden');
        
        $.ajax({
          url: '/admin/delete-product',
          method: 'POST',
          data: {
            id: $product_id
          },
          success: function(res) {
            if (res.message == 'OK') {
              dt.ajax.reload();
              $('#overlay').addClass('hidden');
              swal({
                title: 'Product deleted successfully',
                type: 'success'
              });
            }
            else {
              $('#overlay').addClass('hidden');
              swal({
                title: 'Error',
                text: res.result,
                type: 'error'
              });
            }
          }
        });
      });
    });
    
    // action edit button
    $(document).on('click', '#btnEditProduct', function(event) {
      event.preventDefault();
      $('#overlay').removeClass('hidden');
      $('#btnSaveProduct').removeClass('hidden');
      $('#btnStoreProduct').addClass('hidden');
      var $product_id = this.getAttribute('product_id');
      
      $.ajax({
        url: '/admin/get-product-by-id',
        method: 'POST',
        data: {
          id: $product_id
        },
        success: function(res) {
          if (res.message == 'OK') {
            $('#productModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#overlay').addClass('hidden');
            $('#productId').val(res.result.id);
            $('#name').val(res.result.name);
            CKEDITOR.instances.description.setData(res.result.description);
            CKEDITOR.instances.description_en.setData(res.result.description_en);
            $('#sub_category_id').val(res.result.sub_category_id);
            $('#sub_category_id').trigger('chosen:updated');
            $('#image_thumb').attr('src', res.result.image);
            $('#image_thumb').removeClass('hidden');
            
          }
          else {
            $('#overlay').addClass('hidden');
            swal({
              title: 'Error',
              text: res.result,
              type: 'error'
            });
          }
        }
      });
    });
    
    // form update button
    $(document).on('click', '#btnSaveProduct', function(event) {
      event.preventDefault();
      $('#overlay').removeClass('hidden');
      
      var fd = new FormData();
      fd.append('id', $('#productId').val());
      fd.append('name', $('#name').val());
      fd.append('description', CKEDITOR.instances.description.getData());
      fd.append('description_en', CKEDITOR.instances.description_en.getData());
      fd.append('sub_category_id', $('#sub_category_id').val());
      if(document.getElementById('image').files[0]) {
        fd.append('image', document.getElementById('image').files[0]);
      }
      
      $.ajax({
        url: '/admin/update-product',
        method: 'POST',
        processData: false,
        contentType: false,
        data: fd,
        success: function(res) {
          if (res.message == 'OK') {
            dt.ajax.reload();
            cleanForm();
            $('#overlay').addClass('hidden');
            $('#productModal').modal('hide');
            swal({
              title: 'Product updated successfully',
              type: 'success'
            });
          }
          else {
            $('#overlay').addClass('hidden');
            swal({
              title: 'Error',
              text: res.result.name ? res.result.name : res.result.description ? res.result.description : res.result.description_en ? res.result.description_en : res.result.sub_category_id ? res.result.sub_category_id : res.result,
              type: 'error'
            });
          }
        }
      });
    });
    
    // get input image base64
    function readImage() {
        if ( this.files && this.files[0] ) {
            var FR = new FileReader();
            FR.onload = function(e) {
                $('#image_thumb').removeClass('hidden');
                $('#image_thumb').attr( "src", e.target.result );
            };       
            FR.readAsDataURL( this.files[0] );
        }
    }
    
    function cleanForm() {
      $('#productId').val('');
      $('#name').val('');
      CKEDITOR.instances.description.setData('');
      CKEDITOR.instances.description_en.setData('');
      $('#sub_category_id').val('');
      $('#image_thumb').attr('src','');
      $('#image_thumb').addClass('hidden');
      $('#image').val('');
    }
    
    // on input file source change read input
    $(document).on('change', '#image', readImage);
      
    });
    
  </script>
@stop