<?php

    return [
    
        // NAV PAGES
        'menu.home' => 'Home',
        'menu.about' => 'About',
        'menu.quality' => 'Quality',
        'menu.products' => 'Products',
        'menu.gallery' => 'Gallery',
        'menu.logistics' => 'Services',
        'menu.data_bank' => 'Data bank',
        'menu.news' => 'News',
        'menu.contact' => 'Contact',
        'menu.language' => 'Language',
        
        // PAGE TITLES
        'page.home' => 'Home',
        'page.about' => 'About Us',
        'page.quality' => 'Quality',
        'page.products' => 'Products',
        'page.gallery' => 'Gallery',
        'page.logistics' => 'Services',
        'page.data_bank' => 'Data bank',
        'page.news' => 'News',
        'page.contact' => 'Contact',
        'page.errors' => 'Error',
        
        //BUTTONS
        'button.seemore' => 'see more',
        
        // PAGES
        'home.newProducts' => 'New products',
        'home.testimonies' => 'Testimonies',
        'home.partners' => 'Our Partners',
        'home.news' => 'Latest News',
        'home.h1' => 'Hospital Forniture',
        'home.h2' => 'Medical Devices',
        'home.h3' => 'Cosmetic Care',
        'home.about1' => 'We are not just a supplier but a partner for every need of your structure.',
        'home.about2' => 'We have chosen a list that have consistently delivered, excellent quality products over years.',
        'home.about3' => 'For everyone who is seeking first-class services that combine the use of the latest techniques.',
        'contact.visit-us' => 'VISIT US',
        'contact.contact' => 'CONTACT',
        'contact.input.name' => 'Name*',
        'contact.input.email' => 'Email*',
        'contact.input.subject' => 'Subject',
        'contact.input.message' => 'Message*',
        'contact.button.send' => 'Send',
        'products.products-categories' => 'CATEGORIES',
        'products.product-description' => 'PRODUCT DESCRIPTION',
        'products.product-details' => 'PRODUCT DETAILS',
        'products.related-products' => 'RELATED PRODUCTS',
        'products.product-details-category' => 'Category',
        'products.product-details-date' => 'Date',
        'products.search' => 'No products found with this search terms !',
        'products.search2' => 'Try to search with other ones.',
        'products.back' => 'Back to home',
        'articles.article-description' => 'DESCRIPTION',
        
        // FOOTER
        'footer.contact' => 'Contact',
        'footer.recent-products' => 'Recent Products',
        'footer.links' => 'Links',
        'footer.social' => 'Social'
    
    ]

?>