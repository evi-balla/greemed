<footer id="colophon" class="site-footer" role="contentinfo">
    <div id="supplementary">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <aside class="widget widget-about">
                        <img class="brand" alt="Brand" src="/img/greemed_logo.png" width="145">
                        <p>GREEMED S.A</p>
                        <p>Medical Solutions</p>
                        <p>Your Health Partner</p>
                        <p>Adress: Rr. “Ramazan Demneri” Nd. 1 H.</p>
                        <p>Njesia Bashkiake Nr.5 Kodi Postar 1022</p>
                        <p>Tirana – Albania</p>
                    </aside>
                    <!-- /.widget-about -->
                    <aside class="widget widget-social">
                        <h5 class="widget-title">Stay Connected</h5>
                        <ul class="social-links">
                            <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a class="google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>
                        </ul>
                    </aside>
                    <!-- /.widget-social -->
                </div>
                <!-- /.col-md-3 -->
                <div class="col-md-3">
                    <aside class="widget widget-posts">
                        <div class="subpage-title">
                            <h5>Recent Posts</h5> </div>
                        <ul class="recent-posts">
                            @foreach($products->slice(0,3) as $product)
                            <li> <img src="{{ $product->image }}" alt="{{ $product->name }}">
                                <h5>
                                    <a href="#">{{ $product->name }}</a>
                                    <small><i class="fa fa-clock-o"></i> {{ date("d F Y",strtotime($product->created_at)) }}</small>
                                </h5>
                            </li>
                            @endforeach
                            
                        </ul>
                    </aside>
                    <!-- /.widget-posts -->
                </div>
                <!-- /.col-md-3 -->
                <div class="col-md-3">
                    <aside class="widget widget-tagcloud">
                        <div class="subpage-title">
                            <h5>Tags</h5> </div>
                        <ul class="tag-links">
                            <li><a href="#">photoshop</a></li>
                            <li><a href="#">html</a></li>
                            <li><a href="#">css</a></li>
                            <li><a href="#">html5</a></li>
                            <li><a href="#">css3</a> </li>
                            <li><a href="#">illustration</a></li>
                            <li><a href="#">javascript</a></li>
                            <li><a href="#">jquery</a></li>
                            <li><a href="#">bootstrap 2</a></li>
                            <li><a href="#">bootstrap 3</a></li>
                            <li><a href="#">php</a></li>
                            <li><a href="#">mysql</a></li>
                            <li><a href="#">wordpress</a></li>
                            <li><a href="#">ajax</a></li>
                            <li><a href="#">webmatrix</a></li>
                        </ul>
                    </aside>
                    <!-- /.widget-tagcloud -->
                </div>
                <!-- /.col-md-3 -->
                <div class="col-md-3">
                    <aside class="widget widget-flickr">
                        <div class="subpage-title">
                            <h5>Flickr Photos</h5> </div>
                        <ul class="flickr-photos-list"></ul>
                    </aside>
                    <!-- /.widget-flickr -->
                </div>
                <!-- /.col-md-3 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /#supplementary -->
    <div id="site-info">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="footer-info-wrapper"> <span>Emphatic. © 2014. All Rights Reserved. v1.0.0 currently!</span> </div>
                </div>
                <!-- /.footer-info-wrapper -->
                <div class="col-xs-12 col-sm-6">
                    <div class="footer-links-wrapper">
                        <ul class="list-inline">
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms Of Service</a></li>
                            <li><a href="#">Disclaimer</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /.footer-links-wrapper -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.site-footer -->
</footer>