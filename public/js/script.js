$(document).ready(function () {
    $('.flickr-photos-list').jflickrfeed({
        limit: 9,
        qstrings: {
            id: '71865026@N00'
        },
        itemTemplate: '<li><a href="{{image_b}}"><img src="{{image_s}}" alt="{{title}}" /></a></li>'
    });
    $().UItoTop({ easingType: 'easeOutQuart' });
    // PrettyPhoto
    $("a[data-rel^='prettyPhoto']").prettyPhoto({
        theme: 'light_square',
        social_tools: false,
        hook: 'data-rel'
    });

    // Revolution Slider
    $('.tp-banner').revolution({
        delay: 9000,
        startwidth: 1500,
        startheight: 600,
        hideThumbs: 10,
        fullWidth: "on",
        forceFullWidth: "on",
        navigationStyle: "preview1"
    });
});

// $ CarouFredSel
var caroufredsel = function () {
    $('#caroufredsel-products-container').carouFredSel({
        responsive: true,
        scroll: 1,
        circular: false,
        infinite: false,
        items: {
            visible: {
                min: 1,
                max: 4
            }
        },
        prev: '#products-prev',
        next: '#products-next',
        auto: {
            play: false
        }
    });
    $('#caroufredsel-blog-posts-container').carouFredSel({
        responsive: true,
        scroll: 1,
        circular: false,
        infinite: false,
        items: {
            visible: {
                min: 1,
                max: 3
            }
        },
        prev: '#blog-posts-prev',
        next: '#blog-posts-next',
        auto: {
            play: false
        }
    });
    $('#caroufredsel-partners-container').carouFredSel({
        responsive: true,
        scroll: 1,
        circular: true,
        infinite: true,
        items: {
            visible: {
                min: 1,
                max: 4
            }
        },
        prev: '#partners-prev',
        next: '#partners-next',
        auto: {
            play: false
        }
    });
    $('#caroufredsel-news-container').carouFredSel({
        responsive: true,
        scroll: 1,
        circular: false,
        infinite: false,
        items: {
            visible: {
                min: 1,
                max: 1
            }
        },
        prev: '#news-prev',
        next: '#news-next',
        auto: {
            play: false
        }
    });
};

// Isotope Portfolio
var $container = $('.portfolio-container');
var $blogcontainer = $('.posts-wrap');
var $filter = $('.portfolio-filter');

$(window).load(function () {
    caroufredsel();
    // Initialize Isotope
    $container.isotope({
        itemSelector: '.portfolio-item-wrapper'
    });
    $blogcontainer.isotope({
        itemSelector: '.article-wrap'
    });
    $('.portfolio-filter a').click(function () {
        var selector = $(this).attr('data-filter');
        $container.isotope({ filter: selector });
        return false;
    });
    $filter.find('a').click(function () {
        $filter.find('a').parent().removeClass('active');
        $(this).parent().addClass('active');
    });
});

$(window).resize(function () {
    caroufredsel();
});  