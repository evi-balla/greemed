@extends('backend.layouts.main')
@section('main_content')
<div class="row">
  <div class="col-xs-12 col-md-6">
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Create new article</h3> </div>
      <!-- /.box-header -->
      <!-- form start -->
      <form role="form" name="articleForm">
        <div class="box-body">
          <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control hidden" id="articleId">
            <input type="title" class="form-control" id="title" placeholder="Enter article title">
          </div>
          <div class="form-group">
            <label for="description">Description</label>
            <textarea rows="4" name="description" id="description" class="form-control">
            </textarea>
          </div>
          <div class="form-group">
            <label for="name">Image</label>
            <div class="text-center">
              <input type="file" name="image" accept="image/*" class="form-control" id="image" class="form-control">
              <img src="" class="hidden" id="image_thumb"></img>
            </div>
          </div>
          <div class="form-group">
            <label for="featured">Featured</label>
            <select name="featured" id="featured" class="form-control">
              <option value="0" selected="selected">No</option>
              <option value="1">Yes</option></option>
            </select>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button id="btnCreateArticle" type="submit" class="btn btn-success">Create</button>
          <button id="btnUpdateArticle" type="submit" class="btn btn-warning hidden">Update</button>
          <button id="btnCancelUpdate" type="submit" class="btn btn-danger hidden">Cancel</button>
        </div>
      </form>
    </div>
  </div>
  <div class="col-xs-12 col-md-6">
    <div class="box box-dafault">
      <div class="box-header with-border">
        <h3 class="box-title">List of articles</h3>
        <div class="box-tools pull-right">
          <div class="input-group" style="width: 150px;">
            <input type="text" name="table_search" id="table_search" class="form-control input-sm pull-right" placeholder="Search">
            <div class="input-group-btn">
              <button id="btnSearchArticle" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-responsive table-condensed table-hover" id="articleTable">
          <thead>
            <tr>
              <th>ID</th>
              <th>Title</th>
              <th>Featured</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody> </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer text-center">
        <div id="nav-pag"></div>
      </div>
      <!-- /.box-footer -->
    </div>
  </div>
    
  <style type="text/css">
    .pagination {
        margin: 0;
    }
    #image_thumb {
        max-height: 100px;
        margin: 10px;
        padding: 5px;
        border: solid 1px #ddd;
    }
  </style>
    
  <script>
  
    // send csrf token on every ajax request
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    
    /* global swal, CKEDITOR */
    
    // array of categories to manipulate
    var articles = [];
    
    // run
    getAllArticles();
    
    $(document).ready(function() {
      CKEDITOR.replace('description');
    });
    
    // form create button
    $('#btnCreateArticle').click(function(event) {
      event.preventDefault();
      
      $('#overlay').removeClass('hidden');
      
      var fd = new FormData();
      fd.append('title', $('#title').val());
      fd.append('description', CKEDITOR.instances.description.getData());
      fd.append('featured', $('#featured').val());
      fd.append('image', document.getElementById('image').files[0]);
      $.ajax({
        url: '/admin/create-article',
        method: 'POST',
        processData: false,
        contentType: false,
        data: fd,
        success: function(res) {
          if (res.message == 'OK') {
            $('#overlay').addClass('hidden');
            articles.unshift(res.result);
            updateTable(articles);
            emptyForm();
            swal({
              title: 'Article created successfully',
              type: 'success'
            });
          }
          else {
            swal({
              title: 'Error',
              text: res.result.title ? res.result.title : res.result.description ? res.result.description : res.result,
              type: 'error'
            });
          }
        }
      });
    });
    
    // action delete button
    $(document).on('click','#btnDeleteArticle', function(event) {
      
      event.preventDefault();
      
      var $article_id = this.getAttribute('articleId');
      
      swal({
        title: "Are you sure?",
        text: "You will not be able to recover this article!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true
      }, function() {
        $('#overlay').removeClass('hidden');
        
        $.ajax({
          url: '/admin/delete-article',
          method: 'POST',
          data: {
            id: $article_id
          },
          success: function(res) {
            if (res.message == 'OK') {
              $('#overlay').addClass('hidden');
              removeArticleFromArray($article_id);
              updateTable(articles);
              swal({
                title: 'Article deleted successfully',
                type: 'success'
              });
            }
            else {
              $('#overlay').addClass('hidden');
              swal({
                title: 'Error',
                text: res.result.article_id,
                type: 'error'
              });
            }
          }
        });
      });
    });
    
    // action edit button
    $(document).on('click', '#btnEditArticle', function(event) {
      event.preventDefault();
      $('#overlay').removeClass('hidden');
      var $article_id = this.getAttribute('articleId');
      
      $.ajax({
        url: '/admin/get-article-by-id',
        method: 'POST',
        data: {
          id: $article_id
        },
        success: function(res) {
          if (res.message == 'OK') {
            $('#overlay').addClass('hidden');
            $('#articleId').val(res.result.id);
            $('#title').val(res.result.title);
            $('#featured').val(res.result.featured);
            $('#image_thumb').attr('src', res.result.image);
            $('#image_thumb').removeClass('hidden');
            CKEDITOR.instances.description.setData(res.result.description);
            $('#btnCreateArticle').addClass('hidden');
            $('#btnCancelUpdate').removeClass('hidden');
            $('#btnUpdateArticle').removeClass('hidden');
          }
          else {
            $('#overlay').addClass('hidden');
            swal({
              title: 'Error',
              text: res.result.article_id,
              type: 'error'
            });
          }
        }
      });
    });
    
    // form update button
    $(document).on('click', '#btnUpdateArticle', function(event) {
      event.preventDefault();
      $('#overlay').removeClass('hidden');
      
      var fd = new FormData();
      fd.append('id', $('#articleId').val());
      fd.append('title', $('#title').val());
      fd.append('description', CKEDITOR.instances.description.getData());
      fd.append('featured', $('#featured').val());
      if(document.getElementById('image').files[0]) {
        fd.append('image', document.getElementById('image').files[0]);
      }
      
      $.ajax({
        url: '/admin/update-article',
        method: 'POST',
        processData: false,
        contentType: false,
        data: fd,
        success: function(res) {
          if (res.message == 'OK') {
            updateArticleInArray(res.result);
            updateTable(articles);
            $('#overlay').addClass('hidden');
            emptyForm();
            CKEDITOR.instances.description.setData('');
            $('#btnCreateArticle').removeClass('hidden');
            $('#btnCancelUpdate').addClass('hidden');
            $('#btnUpdateArticle').addClass('hidden');
            swal({
              title: 'Article updated successfully',
              type: 'success'
            });
          }
          else {
            $('#overlay').addClass('hidden');
            swal({
              title: 'Error',
              text: res.result.title ? res.result.title : res.result.description ? res.result.description : res.result,
              type: 'error'
            });
          }
        }
      });
    });
    
    // form cancel button 
    $(document).on('click', '#btnCancelUpdate', function(event) {
      event.preventDefault();
      emptyForm();
      $('#btnUpdateArticle').addClass('hidden');
      $('#btnCreateArticle').removeClass('hidden');
      $(this).addClass('hidden');
    });
    
    // search button
    $(document).on('click', '#btnSearchArticle', function(event) {
      event.preventDefault();
      $('#overlay').removeClass('hidden');
      
      var $title = $('#table_search').val();
      
      $.ajax({
        url: '/admin/search-article-by-title',
        method: 'POST',
        data: {
          title: $title
        },
        success: function(res) {
          if (res.message == 'OK') {
            $('#overlay').addClass('hidden');
            articles = res.result;
            updateTable(articles);
          }
          else {
            $('#overlay').addClass('hidden');
            swal({
              title: 'Error',
              text: JSON.stringify(res.result),
              type: 'error'
            });
          }
        }
      });
    });
    
    // append new elements to table
    function updateTable(data) {
      var tbody = $('#articleTable tbody');
      emptyTable();
      $(data).each(function(row, element) {
        var tr = $('<tr>');
        var tdId = "<td>" + element.id + "</td>";
        var tdTitle = "<td>" + element.title + "</td>";
        var tdFeatured = "<td>" + (element.featured == 1 ? 'Yes' : 'No') + "</td>";
        var tdActions = "<td><div class='dropdown'>" + "<button class='btn btn-xs btn-primary dropdown-toggle' data-toggle='dropdown'>" + "Actions <span class='caret'></span>" + "</button>" + "<ul class='dropdown-menu'>" + "<li role='presentation'><a id='btnEditArticle' articleId='" + element.id + "' role='menuitem' tabindex='-1'><i class='fa fa-edit'></i> Edit</a></li>" + "<li role='presentation'><a id='btnDeleteArticle' articleId='" + element.id + "' role='menuitem' tabindex='-1'><i class='fa fa-remove'></i> Delete</a></li>" + "</ul>" + "</div></td>";
        tr.append(tdId, tdTitle, tdFeatured, tdActions);
        tbody.append(tr);
      });
      pagginateTable();
    }
    
    // empty table
    function emptyTable(el) {
      if (typeof el != 'undefined') {
        el.innerHTML = "";
        return;
      }
      var tbody = $('#articleTable tbody');
      tbody.empty();
      return;
    }
    
    // paginate table
    function pagginateTable() {
      $('#articleTable').each(function() {
        var currentPage = 0;
            var numPerPage = 10;
            var $table = $(this);
            $table.bind('repaginate', function() {
                $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
            });
            $table.trigger('repaginate');
            var numRows = $table.find('tbody tr').length;
            var numPages = Math.ceil(numRows / numPerPage);
            var $pager = $('<ul class="pagination"></ul>');
            
            for (var page = 0; page < numPages; page++) {
                
                var $beginLi = '<li pageNr="' + (page) + '"class="page">'  ;
                var $anchor =  $beginLi + '<a href="#">'+ (page + 1) + '</a>';
                var $endLi = '</li>';
                var $line = $anchor + $endLi;
                $pager.append($line);
            } 
            	
            $( ".pagination" ).remove();
            $('#nav-pag').append($pager).find('li.page:first').addClass('active');
            
            $(".page").click(function() {
                    var newPage = $(this).attr('pagenr');
                    currentPage = newPage;
                    $table.trigger('repaginate');
                    $(this).addClass('active').siblings().removeClass('active');
            })
      });
    }
    
    // get all categories 
    function getAllArticles() {
      $('#overlay').removeClass('hidden');
      $.ajax({
        url: '/admin/get-all-articles',
        method: 'POST',
        success: function(res) {
          if (res.message == 'OK') {
            $('#overlay').addClass('hidden');
            for(var a in res.result) {
              articles.push(res.result[a]);
            }
            updateTable(articles);
          }
          else {
            swal({
              title: 'Error',
              text: res.result,
              type: 'error'
            });
          }
        }
      });
    }
    
    // get obj index in array
    function removeArticleFromArray(id) {
      $.each(articles, function(index, object) {
          if(object.id == parseInt(id)) {
              articles.splice(index, 1);
              return false; // exit loop
          }
      })
    }
    
    // update object attribute in array
    function updateArticleInArray(article) {
      $.each(articles, function(index, object) {
          if(object.id == parseInt(article.id)) {
              object.title = article.title;
              object.description = article.description;
              object.image = article.image;
              object.featured = article.featured;
              return false; // exit loop
          }
      })
    }
    
    // get input image base64
    function readImage() {
        if ( this.files && this.files[0] ) {
            var FR = new FileReader();
            FR.onload = function(e) {
                $('#image_thumb').removeClass('hidden');
                $('#image_thumb').attr( "src", e.target.result );
            };       
            FR.readAsDataURL( this.files[0] );
        }
    }
    
    // on input file source change read input
    $(document).on('change', '#image', readImage);
    
    // empty form
    function emptyForm() {
        $('#articleId').val('');
        $('#title').val('');
        $('#featured').val('0');
        $('#image').val('');
        $('#image_thumb').attr('src', '');
        $('#image_thumb').addClass('hidden');
        CKEDITOR.instances.description.setData('');
    }
    
    
  </script>
    
</div>
@stop