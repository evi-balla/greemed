<div class="section type5">
    <div class="tp-banner-container">
        <div class="tp-banner">
            <ul>
                <li data-transition="random" data-slotamount="7" data-masterspeed="700" data-title="Responsive Design">
                    <img src="img/slider/slides.jpg" alt="Greemed | Your health partner" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat"> 
                </li>
                <li data-transition="random" data-slotamount="7" data-masterspeed="700" data-title="Responsive Design">
                    <img src="img/slider/slidesRER.jpg" alt="Greemed | Your health partner" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat"> 
                </li>
                <!--<li data-transition="random" data-slotamount="7" data-masterspeed="700" data-title="Responsive Design">-->
                <!--    <img src="img/slider/slider-3.jpg" alt="Greemed | Your health partner" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat"> -->
                <!--</li>-->
                <!--<li data-transition="random" data-slotamount="7" data-masterspeed="700" data-title="Responsive Design">-->
                <!--    <img src="img/slider/slider-4.jpg" alt="Greemed | Your health partner" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat"> -->
                <!--</li>-->
                <!--<li data-transition="random" data-slotamount="7" data-masterspeed="700" data-title="Responsive Design">-->
                <!--    <img src="img/slider/slider-5.jpg" alt="Greemed | Your health partner" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat"> -->
                </li>
            </ul>
            <div class="tp-bannertimer tp-bottom"></div>
        </div>
    </div>
</div>