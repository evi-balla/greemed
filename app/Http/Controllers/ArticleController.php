<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \App\Article as Article;
use Input;
use Image;
use Config;
use Exception;
use Validator;

class ArticleController extends BaseController
{

    public function index()
    {
        try {
            
            $articles = Article::all();
            
            return $this->doResponse('OK', $articles, 200);

            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }

    public function store(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'title' => 'required',
                    'description' => 'required'
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 400);
            }
            
            $article = new Article;
            $article->title = $request->input('title');
            $article->description = $request->input('description');
            
            if(Input::exists('featured')) {
                $article->featured = $request->input('featured');
            }
            
            if(Input::exists('image')) {
                
                $image = Image::make(Input::file('image'));
                $image_name = $this->whash() . '.' . strtolower(Input::file('image')->getClientOriginalExtension());
                $image_destination = Config::get('app.public_path') . $this->articlesStoragePath();
                $image_path = $image_destination . '/' . $image_name;
                $image->save($image_path);   
                
                $article->image = $this->articlesStoragePath() . '/' . $image_name;
            }
            
            $article->save();
            
            return $this->doResponse('OK', $article, 200);
            
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }

    public function update(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'id'   => 'required|exists:articles,id',
                    'title' => 'required',
                    'description' => 'required',
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 400);
            }
            
            $article = Article::find($request->input('id'));
            $article->title = $request->input('title');
            $article->description = $request->input('description');
            
            
            if(Input::exists('featured')) {
                $article->featured = $request->input('featured');
            }
            
            if(Input::get('image') != 'undefined') {
                
                // delete old image
                
                if($article->image) {
                    unlink(Config::get('app.public_path') . $article->image);
                }
                
                $image = Image::make(Input::file('image'));
                $image_name = $this->whash() . '.' . strtolower(Input::file('image')->getClientOriginalExtension());
                $image_destination = Config::get('app.public_path') . $this->articlesStoragePath();
                $image_path = $image_destination . '/' . $image_name;
                $image->save($image_path);   
                
                $article->image = $this->articlesStoragePath() . '/' . $image_name;
            }
            
            
            $article->save();
            
            return $this->doResponse('OK', $article, 200);
            
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }

    public function destroy(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'id'   => 'required|exists:articles,id',
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 400);
            }
            
            $article = Article::find($request->input('id'));
            
            if($article->image) {
                unlink(Config::get('app.public_path') . $article->image);
            }
            
            $article->delete();
            
            return $this->doResponse('OK', $article, 200);
            
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    public function getAllArticles()
    {
        try {
            
            $articles = Article::all();
            
            return $this->doResponse('OK', $articles , 200);
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    public function getArticleById(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'id' => 'required|exists:articles,id'
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 401);
            }
            
            $article = Article::find($request->input('id'));
            
            return $this->doResponse('OK', $article , 200);
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
    public function searchArticleByTitle(Request $request)
    {
        try {
            
            if(strlen($request->input('title')) > 0 ) {
                $res = Article::where('title', 'like', '%'.$request->input('title').'%')->get();
            } else {
                $res = Article::all();
            }
            
            return $this->doResponse('OK', $res , 200);
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
}
