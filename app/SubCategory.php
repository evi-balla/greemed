<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class SubCategory extends Model implements SluggableInterface
{
    protected $table = 'sub_categories';
    
    protected $fillable = ['name', 'slug'];
    
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
    ];
    
    
    public function category() {
        return $this->belongsTo('App\Category');
    }
    
    public function products() {
        return $this->hasMany('App\Product', 'sub_category_id', 'id');
    } 
}
