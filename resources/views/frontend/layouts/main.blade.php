<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Medical Devices, Hospital Forniture, Cosmetic Care">
        <meta name="author" content="">
        <link rel="shortcut icon" href="/ico/favicon.ico">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @if($pageTitle == 'Home')
        <title>Greemed | Your health partner</title>
        @else
        <title>{{ $pageTitle }} - Greemed | Your health partner</title>
        @endif
        <meta name="description" content="Medical Equipments, Devices and Consumables" />
        <meta name="keywords" content="Medical Equipments, Devices and Consumables" />
        <meta property="og:title" content="Greemed | Your health partner">
        <meta property="og:site_name" content="greemed.eu">
        <meta property="og:url" content="http://www.greemed.eu">
        <meta property="og:description" content="Medical Equipments, Devices and Consumables">
        <meta property="og:image" content="">
         <!--Bootstrap Core CSS -->
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/plugins/prettyPhoto/css/prettyPhoto.css" rel="stylesheet">
        <link href="/css/animate.min.css" rel="stylesheet">
        <link href="/css/ui.totop.css" rel="stylesheet">
         <!--SLIDER REVOLUTION 4.x CSS SETTINGS -->
        <link href="/css/extralayers.min.css" rel="stylesheet">
        <link href="/plugins/rs-plugin/css/settings.css" rel="stylesheet">
         <!--Font Awesome  -->
        <link href="/css/font-awesome.min.css" rel="stylesheet">
         <!--Custom Stylesheet For This Template -->
        <!--<link href="/css/stylesheet.css" rel="stylesheet">-->
        <link href="/css/stylesheet.min.css" rel="stylesheet">
        <link href="/css/skins.min.css" rel="stylesheet">
         <!--Google Fonts -->
        <!--<link type="text/css" rel="stylesheet" href="/min/f=bootstrap/css/bootstrap.min.css,plugins/prettyPhoto/css/prettyPhoto.css,css/animate.min.css,css/ui.totop.css,css/extralayers.css,plugins/rs-plugin/css/settings.css,css/font-awesome.min.css,css/stylesheet.css,css/skins.css,bower_components/lightgallery/dist/css/lightgallery.min.css" />-->
        <!--<link href="https://fonts.googleapis.com/css?family=Muli:200i,300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet" type="text/css">-->
        <!--<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet" type="text/css">-->
        <!--<link href="http://fonts.googleapis.com/css?family=Raleway:400,900,800,700,600,500,300,200,100" rel="stylesheet" type="text/css">-->
        <link href="https://fonts.googleapis.com/css?family=Arimo:400,400i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
        <link rel="stylesheet" href="/bower_components/photoswipe/dist/photoswipe.css">
        <link rel="stylesheet" href="/bower_components/photoswipe/dist/default-skin/default-skin.css">
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="js/html5shiv.min.js"></script>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        <script src="/js/jquery-2.1.4.min.js"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-104751114-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'UA-104751114-1');
        </script>

    </head>
    <!--<body class="color-skin-1 color-pattern-1 retouch-background" style="background: url('/img/patterns/greemed-bg.jpg')">-->
    <body class="color-skin-1 color-pattern-1 retouch-background" style="background: #fff;">
        <div id="page" class="hfeed site">
            @yield('nav')
            @yield('page_title')
            
            <!-- /#masthead -->
            <div id="main" class="wrapper">
                @yield('slider')
                <div class="container">
                    <div id="primary" class="site-comtent">
                        @yield('main_container')
                    </div>
                </div>
                 <div class="container-fluid" style="padding: 0px;"> 
                    @yield('partners')
                 </div>
                
            </div>
            <!-- /#main -->
            <footer id="colophon" class="site-footer" role="contentinfo">
                <div id="supplementary">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <aside class="widget widget-about"> 
                                    <div class="subpage-title">
                                        <h5>{{Lang::get('translations.footer.contact')}}</h5> 
                                    </div>
                                    <p>GREEMED S.A</p>
                                    <p>Medical Solutions</p>
                                    <p>Your Health Partner</p>
                                    <p>Rr. “Ramazan Demneri” Nd. 1 H.</p>
                                    <p>Njesia Bashkiake Nr.5 Kodi Postar 1022</p>
                                    <p>Tirana – Albania</p>
                                </aside>
                            </div>
                            <!-- /.col-md-3 -->
                            <div class="col-md-4 col-sm-4 col-xs-12 text-left">
                                <aside class="widget widget-posts widget-products">
                                    <div class="subpage-title">
                                        <h5>{{Lang::get('translations.footer.recent-products')}}</h5> </div>
                                    <ul class="recent-posts">
                                        @foreach($products->slice(0,4) as $product)
                                        <li>
                                             <!--<img src="{{ $product->image }}" alt="{{ $product->name }}">-->
                                            <h5>
                                                <a href="/product/{{$product->slug}}" target="_blank">{{ $product->name }}</a>
                                                <small><i class="fa fa-clock-o"></i> {{ date("d F Y",strtotime($product->created_at)) }}</small>
                                            </h5>
                                        </li>
                                        @endforeach
                                        
                                    </ul>
                                </aside>
                                <!-- /.widget-posts -->
                            </div>
                            <!-- /.col-md-3 -->
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <aside class="widget widget-tagcloud">
                                    <div class="subpage-title">
                                        <h5>{{Lang::get('translations.footer.links')}}</h5> </div>
                                    <!--<ul class="tag-links">-->
                                    <ul class="footer-links">
                                        <li><a href="/about">{{ Lang::get('translations.menu.about') }}</a></li>
                                        <li><a href="/quality">{{ Lang::get('translations.menu.quality') }}</a></li>
                                        <li><a href="/products">{{ Lang::get('translations.menu.products') }}</a></li>
                                        <li><a href="/gallery">{{ Lang::get('translations.menu.gallery') }}</a></li>
                                        <li><a href="/logistics">{{ Lang::get('translations.menu.logistics') }}</a></li>
                                        <!--<li><a href="/data-bank">{{ Lang::get('translations.menu.data_bank') }}</a></li>-->
                                        <li><a href="/news">{{ Lang::get('translations.menu.news') }}</a></li>
                                        <li><a href="/contact">{{ Lang::get('translations.menu.contact') }}</a></li>
                                    </ul>
                                </aside>
                                <!-- /.widget-tagcloud -->
                            </div>
                            <!-- /.col-md-3 -->
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <!-- /.widget-about -->
                                <aside class="widget widget-social">
                                    <h5 class="widget-title">{{Lang::get('translations.footer.social')}}</h5>
                                    <ul class="social-links">
                                        <li><a class="facebook" href="https://www.facebook.com/greemed.eu/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                        <li><a class="facebook" href="https://www.instagram.com/greemed_al/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                        <!--<li><a class="google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>-->
                                        <!--<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>-->
                                        <!--<li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>-->
                                        <!--<li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>-->
                                    </ul>
                                </aside>
                                <!-- /.widget-social -->
                                <!-- /.widget-flickr -->
                            </div>
                            <!-- /.col-md-3 -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container -->
                </div>
                <!-- /#supplementary -->
                <div id="site-info">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <div class="footer-info-wrapper"> <p>Greemed. © <?php echo date("Y"); ?>. All Rights Reserved.</p> </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 text-center">
                                <a href="/pdf/GREEMED.CERT.15-Q-0200619-TIC.pdf" target="_blank">
                                    <img src="/img/tuv.png" class="img-responsive iso-certificate" width="80px" />    
                                    <span>ISO 9001:2008</span>
                                 </a>
                            </div>
                            <!-- /.footer-info-wrapper -->
                            <div class="col-xs-12 col-sm-4">
                                <div class="footer-links-wrapper footer-info-wrapper">
                                    <ul class="list-inline">
                                        <li><a href="#">Privacy Policy</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.footer-links-wrapper -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container -->
                </div>
                <!-- /.site-footer -->
            </footer>
            <!-- /#site-info -->
        </div>
         <!--/#page -->
         <!--Start of Tawk.to Script-->
            <script type="text/javascript">
                var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
                (function(){
                var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                s1.async=true;
                s1.src='https://embed.tawk.to/5cd2f9cc2846b90c57ad8931/default';
                s1.charset='UTF-8';
                s1.setAttribute('crossorigin','*');
                s0.parentNode.insertBefore(s1,s0);
                })();
            </script>
        <!--End of Tawk.to Script-->
         <!--Bootstrap JS & Others JavaScript Plugins -->
         <!--Placed At The End Of The Document So Page Loads Faster -->
        
        <script src="/js/jquery-migrate-1.2.1.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
         <!--SLIDER REVOLUTION 4.x SCRIPTS  -->
        <script src="/plugins/rs-plugin/js/jquery.themepunch.tools.min.js" defer></script>
        <script src="/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js" defer></script>
        <script src="/plugins/prettyPhoto/js/jquery.prettyPhoto.js" defer></script>
        <script src="/js/jquery.carouFredSel-6.2.1-packed.js" defer></script>
        <script src="/js/jflickrfeed.min.js" defer></script>
        <script src="/js/easing.min.js" defer></script>
        <script src="/js/jquery.ui.totop.min.js" defer></script>
        <script src="/js/isotope.pkgd.min.js" defer></script>
        <script src="/js/jquery.fitvids.min.js" defer></script>
        <script src="/bower_components/photoswipe/dist/photoswipe.min.js" defer></script>
        <script src="/bower_components/photoswipe/dist/photoswipe-ui-default.min.js" defer></script>
         <!--Custom Script For This Template -->
        <script src="/js/jquery.cookie.min.js" defer></script>
        <script src="/js/script.js" defer></script>
        <!--<script type="text/javascript" src="/min/f=js/jquery-2.1.4.min.js,js/jquery-migrate-1.2.1.min.js,js/bootstrap.min.js,plugins/rs-plugin/js/jquery.themepunch.tools.min.js,plugins/rs-plugin/js/jquery.themepunch.revolution.min.js,plugins/prettyPhoto/js/jquery.prettyPhoto.js,js/jquery.carouFredSel-6.2.1-packed.js,js/jflickrfeed.min.js,js/easing.js,js/jquery.ui.totop.min.js,js/isotope.pkgd.min.js,js/jquery.fitvids.js,bower_components/lightgallery/dist/js/lightgallery.min.js,bower_components/lightgallery/dist/js/lg-thumbnail.min.js,js/script.js"></script>-->
    </body>
</html>

