@extends('backend.layouts.main')
@section('main_content')
<div class="row">
    <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>{{ count($categories) }}</h3>
                <p>Categories</p>
            </div>
            <div class="icon"></div> <a href="/admin/categories" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a> </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>{{ count($sub_categories) }}</h3>
                <p>Sub Categories</p>
            </div>
            <div class="icon"></i> </div> <a href="/admin/sub-categories" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a> </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>{{ count($products) }}</h3>
                <p>Products</p>
            </div>
            <div class="icon"></i> </div> <a href="/admin/products" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a> </div>
    </div>
    <!-- ./col -->
</div>
<div class="row">
    <div class="col-xs-6">
        <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Recently Added Products</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <ul class="products-list product-list-in-box">
            @foreach($products as $product)
            <li class="item">
              <div class="product-img">
                <img src="{{ $product->image }}" alt="Product Image">
              </div>
              <div class="product-info">
                <a href="#" class="product-title">{{ $product->name }}</a>
                <span class="product-description">
                  {!! $product->description !!}
                </span>
              </div>
            </li>                      
            @endforeach
          </ul>
        </div><!-- /.box-body -->
        <div class="box-footer text-center">
          <a href="/admin/products" class="uppercase">View All Products</a>
        </div><!-- /.box-footer -->
      </div>
    </div>
</div>
@stop