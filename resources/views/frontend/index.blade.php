@extends('frontend.layouts.main')

@section('nav')
    @include('frontend.partials.nav')
@stop

@section('slider')
    @include('frontend.partials.slider')
@stop

@section('main_container')
<!-- Latest Products Section -->
<section id="about-three" class="section type5">
    <div class="row">
        <div class="col-md-4 text-center about-col">
            <div class="about-img-div">
                <img src="img/hp/h1.png" class="img-responsive about-img">
            </div>
            <h2 class="about-titles">{{ Lang::get('translations.home.h1') }}</h2>
            <p class="about-desc">{{ Lang::get('translations.home.about1') }}</p>
            <a href="/products" class="about-link">{{ Lang::get('translations.button.seemore') }}</a>
        </div>
        <div class="col-md-4 text-center about-col">
             <div class="about-img-div">
                <img src="img/hp/h3.png" class="img-responsive about-img">
            </div>
            <h2 class="about-titles">{{ Lang::get('translations.home.h2') }}</h2>
            <p class="about-desc">{{ Lang::get('translations.home.about2') }}</p></p>
            <a href="/products" onclick="saveMedicalDevices()" class="about-link">{{ Lang::get('translations.button.seemore') }}</a>
        </div>
        <div class="col-md-4 text-center about-col">
             <div class="about-img-div">
                <img src="img/hp/h2.png" class="img-responsive about-img">
            </div>
            <h2 class="about-titles">{{ Lang::get('translations.home.h3') }}</h2>
            <p class="about-desc" >{{ Lang::get('translations.home.about3') }}</p>
            <a href="/products" onclick="saveCosmeticCare()" class="about-link">{{ Lang::get('translations.button.seemore') }}</a>
        </div>
    </div>
</section>

<!-- Latest Products Section -->
<section id="caroufredsel-portfolio" class="section type3">
    <div class="row cnbox">
        <div class="col-md-12 text-center">
            <h1 class="cbox-title">{{ Lang::get('translations.home.newProducts') }}</h1></div>
        </div>
        <!--<div class="col-md-4">-->
        <!--    <div class="cbox">-->
        <!--        <h3 class="cbox-title">{{ Lang::get('translations.home.newProducts') }}</h3> <span class="cbox-counter">10+</span> </div>-->
        <!--</div>-->
        <!--<div class="col-md-8">-->
        <!--    <div class="nbox">-->
        <!--        <div class="controls">-->
        <!--             <span id="products-prev" class="prev black"><i class="fa fa-angle-left"></i></span>-->
        <!--             <span id="products-next" class="next black"><i class="fa fa-angle-right"></i></span>-->
        <!--        </div>-->
        <!--    </div>-->
        <!--</div>-->
    </div>
    <div class="hide-overflow">
        <div class="row">
            <div id="caroufredsel-products-container">
                @foreach($products as $product)
                <div class="col-xs-12 col-sm-6 col-md-3 portfolio-item-wrapper artwork creative">
                    <div class="portfolio-item type2">
                        <div class="portfolio-thumb"> <img style="height: 150px; width: auto; margin: 0 auto;" src="{{ $product->image }}" class="img-responsive" alt="{{ $product->name }}">
                            <div class="image-overlay"></div> <a href="{{ $product->image }}" data-rel="prettyPhoto[pp_gal]" class="portfolio-zoom"><i class="fa fa-plus"></i></a> <a href="/product/{{ $product->slug }}" class="portfolio-link"><i class="fa fa-link"></i></a> </div>
                        <div class="portfolio-details">
                            <div class="portfolio-meta">
                                 <span class="portfolio-date">
                                     <!--<i class="fa fa-clock-o"></i>-->
                                    <!--<a href="#">&nbsp; {{ date("d F Y",strtotime($product->created_at)) }}</a>-->
                                </span>
                            </div>
                            <h5 class="portfolio-title"><a href="/product/{{ $product->slug }}">{{ $product->name }}</a></h5> </div>
                    </div>
                </div>
                @endforeach()
                <!-- /.portfolio-item-wrapper -->
            </div>
            <!-- /.caroufredsel-portfolio-container -->
            <div class="col-md-12 text-center">
                <div class="controls">
                     <span id="products-prev" class="prev black"><i class="fa fa-angle-left"></i></span>
                     <span id="products-next" class="next black"><i class="fa fa-angle-right"></i></span>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
</section>

<section class="video-testimonial">
    <div class="row cnbox">
        <div class="col-md-12 text-center">
            <h1 class="cbox-title">{{ Lang::get('translations.home.testimonies') }}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center padding-top-35">
            <iframe src="https://player.vimeo.com/video/334898406" width="850" height="420" frameborder="0" id="homeVideo" allow="autoplay; fullscreen" allowfullscreen></iframe>
        </div>
    </div>
</section>

<script type="text/javascript">
    function saveMedicalDevices(){
        localStorage.setItem('medicalDevices', true)
    }
    function saveCosmeticCare(){
        localStorage.setItem('cosmeticCare', true)
    }
    
    $( document ).ready(function() {
        localStorage.setItem('medicalDevices', false)
        localStorage.setItem('cosmeticCare', false)
    });
</script>

<!-- Latest News Section-->
<!--<section id="blog-posts" class="section type2">-->
<!--<div class="subpage-title type2">-->
<!--        <h5>{{ Lang::get('translations.home.news') }}</h5>-->
<!--         Controls -->
<!--        <div class="controls"> <span id="news-prev" class="prev"><i class="fa fa-angle-left"></i></span> <span id="news-next" class="next"><i class="fa fa-angle-right"></i></span> </div>-->
<!--    </div>-->
<!--    <div class="hide-overflow">-->
<!--        <div class="row">-->
<!--            <div id="caroufredsel-news-container">-->
                
<!--                @foreach($articles as $article)-->
<!--                <div class="col-xs-12 col-sm-6 col-md-4 post-wrapper">-->
<!--                    <article class="post type3">-->
<!--                        <div class="entry-thumb"> <img style="height: 200px; width: 100%;" src="{{ $article->image }}" class="img-responsive" alt="Responsive image">-->
<!--                            <div class="image-overlay"></div> <a href="{{ $article->image }}" data-rel="prettyPhoto" class="ima-zoom"><i class="fa fa-plus"></i></a> </div>-->
<!--                         /.entry-thumb -->
<!--                        <div class="entry-wrap">-->
<!--                            <div class="entry-meta"> <span class="entry-date"><i class="fa fa-clock-o"></i> <a href="#">{{ date("d F Y",strtotime($article->created_at)) }}</a></span></div>-->
<!--                            <header class="entry-header">-->
<!--                                <h5 class="entry-title"><a href="/article/{{ $article->slug }}">{{ $article->title }}</a></h5> </header>-->
<!--                            <div class="entry-content">-->
<!--                                <p>{!! str_limit($article->description, $limit = 60, $end = '...') !!}</p>-->
<!--                            </div>-->
<!--                             /.entry-content -->
<!--                        </div>-->
<!--                    </article>-->
<!--                </div>-->
<!--                @endforeach-->
                
<!--            </div>-->
<!--             /#caroufredsel-blog-posts-container -->
<!--        </div>-->
<!--         /.row -->
<!--    </div>-->
<!--</section>-->

<!-- Partners Section-->
<!--<section id="our-clients" class="section type2">-->
<!--    <div class="subpage-title type2">-->
<!--        <h5>{{ Lang::get('translations.home.partners') }}</h5>-->
<!--         Controls -->
<!--        <div class="controls"> <span id="partners-prev" class="prev"><i class="fa fa-angle-left"></i></span> <span id="partners-next" class="next"><i class="fa fa-angle-right"></i></span> </div>-->
<!--    </div>-->
<!--    <div class="row flush">-->
<!--        <div id="caroufredsel-partners-container">-->
<!--            <div class="col-xs-12 col-sm-4 col-md-2 client-wrapper">-->
<!--                <a href="https://www.vygon.com/"><img class="img-responsive" src="img/partners/vygon.png" alt="Client Image"></a>-->
<!--            </div>-->
<!--             /.client-wrapper -->
<!--            <div class="col-xs-12 col-sm-4 col-md-2 client-wrapper">-->
<!--                <a href="https://in.erbe-med.com/in-en/"><img class="img-responsive" src="img/partners/erbe.png" alt="Client Image"></a>-->
<!--            </div>-->
<!--             /.client-wrapper -->
<!--            <div class="col-xs-12 col-sm-4 col-md-2 client-wrapper">-->
<!--                <a href="http://www.efer.com/"><img class="img-responsive" src="img/partners/efer.png" alt="Client Image"></a>-->
<!--            </div>-->
<!--             /.client-wrapper -->
<!--            <div class="col-xs-12 col-sm-4 col-md-2 client-wrapper">-->
<!--                <a href="http://www.medster.com.tr/"><img class="img-responsive" src="img/partners/medster.png" alt="Client Image"></a>-->
<!--            </div>-->
<!--             /.client-wrapper -->
<!--            <div class="col-xs-12 col-sm-4 col-md-2 client-wrapper">-->
<!--                <a href="http://www.kimpailac.com/tr/"><img class="img-responsive" src="img/partners/kimpa.jpg" alt="Client Image"></a>-->
<!--            </div>-->
<!--             /.client-wrapper -->
<!--            <div class="col-xs-12 col-sm-4 col-md-2 client-wrapper">-->
<!--                <a href="http://www.erpamedikal.com/"><img class="img-responsive" src="img/partners/erpa.svg" alt="Client Image"></a>-->
<!--            </div>-->
<!--              /.client-wrapper -->
<!--            <div class="col-xs-12 col-sm-4 col-md-2 client-wrapper">-->
<!--                <a href="http://skintact.com/"><img class="img-responsive" src="img/partners/skintact.png" alt="Client Image"></a>-->
<!--            </div>-->
<!--             /.client-wrapper -->
<!--            <div class="col-xs-12 col-sm-4 col-md-2 client-wrapper">-->
<!--                <a href="http://www.maquet.com/"><img class="img-responsive" src="img/partners/maquet.png" alt="Client Image"></a>-->
<!--            </div>-->
<!--             /.client-wrapper -->
<!--            <div class="col-xs-12 col-sm-4 col-md-3 client-wrapper">-->
<!--                <a href="http://www.polymedicure.com/"><img class="img-responsive" src="img/partners/polymed.png" alt="Client Image"></a>-->
<!--            </div>-->
<!--             /.client-wrapper -->
<!--            <div class="col-xs-12 col-sm-4 col-md-2 client-wrapper">-->
<!--                <a href="http://www.trimpeks.com/en/"><img class="img-responsive" src="img/partners/trimpekslogo.png" alt="Client Image"></a>-->
<!--            </div>-->
<!--             /.client-wrapper -->
<!--            <div class="col-xs-12 col-sm-4 col-md-2 client-wrapper">-->
<!--                <a href="http://www.jotec.com/"><img class="img-responsive" src="img/partners/jotec.png" alt="Client Image"></a>-->
<!--            </div>-->
<!--             /.client-wrapper -->
<!--            <div class="col-xs-12 col-sm-4 col-md-2 client-wrapper">-->
<!--                <a href="http://www.brochemedikal.com/"><img class="img-responsive" src="img/partners/broche.png" alt="Client Image"></a>-->
<!--            </div>-->
<!--        </div>-->
<!--         /#caroufredsel-clients-container -->
<!--    </div>-->
<!--     /.row -->
<!--</section>-->

@stop

@section('articles')
    @include('frontend.partials.articles')
@stop

<!--@section('slogan')-->

<!--<div class="row slogan-section">-->
<!--    <div class="col-md-5">-->
<!--        <h1 class="slogan-logo">Greemed</h1>-->
<!--    </div>-->
<!--    <div class="col-md-7 text-left">-->
<!--        <p class="first-sentence">Your Health Partner.</p>-->
<!--        <p class="second-sentence">Building Quality Together.</p>-->
<!--    </div>-->
<!--</div>-->

<!--@stop-->

@section('partners')
    @include('frontend.partials.partners')
@stop