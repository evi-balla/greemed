@extends('frontend.layouts.main') 
@section('nav') 
@include('frontend.partials.nav') 
@stop 
@section('page_title')
@include('frontend.partials.page_title') 
@stop 
@section('main_container')
<style>
    
    .product-image {
        background-size: contain!important;
        width: 100%;
        background-position: 50% 50% !important;
        height: 300px;
        background-repeat: no-repeat!important;
    }
    
</style>
<div id="main" class="wrapper">
    <div id="primary" class="site-comtent">
        <div class="container">
            <!-- /.portfolio-navigation -->
            <div class="row">
                <div class="col-md-6">
                    <section class="portfolio-thumb section type2">
                        <div class="product-image" style="background: url('{{ $product->image }}');"></div>
                    </section>
                </div>
                <div class="col-md-6">
                    <section class="section type2">
                        <div class="subpage-title">
                            <h5>{{ Lang::get('translations.products.product-description') }}</h5>
                        </div>
                        
                        @if ( Config::get('app.locale') == 'en')

                           {!! $product->description_en !!}
                    
                        @elseif ( Config::get('app.locale') == 'al' )
                    
                           {!! $product->description !!}
                    
                        @endif
                        
                        <!--{!! $product->description !!}-->
                    </section>
                    <section class="section type2">
                        <div class="subpage-title">
                            <h5>{{ Lang::get('translations.products.product-details') }}</h5>
                        </div>
                        <ul class="project-details-list">
                            <li>
                                <h6>{{ Lang::get('translations.products.product-details-category') }}:</h6>
                                <div class="project-terms">
                                    {{ $product->subCategory->name}}
                                </div>
                            </li>
                            <!--<li>-->
                            <!--    <h6>{{ Lang::get('translations.products.product-details-date') }}:</h6>-->
                            <!--    <div class="project-terms">-->
                            <!--    {{ date("d F Y",strtotime($product->created_at)) }}-->
                            <!--    </div>-->
                            <!--</li>-->

                        </ul>
                    </section>
                </div>
            </div>
            <!-- /.portfolio-thumb -->

            <section class="section type3">
                <div class="subpage-title type3">
                    <h5>{{ Lang::get('translations.products.related-products') }}</h5>
                    <!-- Controls -->
                    <div class="controls">
                        <span id="portfolio-prev" class="prev black"><i class="fa fa-angle-left"></i></span>
                        <span id="portfolio-next" class="next black"><i class="fa fa-angle-right"></i></span>
                    </div>
                </div>
                <div class="hide-overflow">
                    <div class="row">
                        <div id="caroufredsel-portfolio-container">
                            
                            @foreach($relatedProducts as $rp)
                            
                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio-item-wrapper artwork creative">
                                <div class="portfolio-item type2">
                                    <div class="portfolio-thumb">
                                        <img src="{{ $rp->image }}" class="img-responsive" alt="{{ $rp->name }}">
                                        <a href="/product/{{ $rp->slug }}" class="image-overlay"></a>
                                    </div>
                                    <div class="portfolio-details">
                                        <!--<div class="portfolio-meta">-->
                                        <!--    <span class="portfolio-date"><i class="fa fa-clock-o"></i> <a href="#">{{ date("d F Y",strtotime($rp->created_at)) }}</a></span>-->
                                        <!--</div>-->
                                        <h5 class="portfolio-title"><a href="/product/{{ $rp->slug }}">{{ $rp->name }}</a></h5>
                                    </div>
                                </div>
                            </div>
                            
                            @endforeach
                            
                            
                            <!-- /.portfolio-item-wrapper -->
                        </div>
                        <!-- /.caroufredsel-portfolio-container -->
                    </div>
                    <!-- /.row -->
                </div>

            </section>
        </div>
    </div>
    <!-- /.site-comtent -->
</div>
@stop 