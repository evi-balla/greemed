<?php

    return [
    
        // NAV PAGES
        'menu.home' => 'Kreu',
        'menu.about' => 'Rreth nesh',
        'menu.quality' => 'Cilësia',
        'menu.products' => 'Produktet',
        'menu.gallery' => 'Galeria',
        'menu.logistics' => 'Sherbimet',
        'menu.data_bank' => 'Të dhëna',
        'menu.news' => 'Inovacion',
        'menu.contact' => 'Kontakt',
        'menu.language' => 'Gjuha',
        
        // PAGE TITLES
        'page.home' => 'Kreu',
        'page.about' => 'Rreth nesh',
        'page.quality' => 'Cilësia',
        'page.products' => 'Produktet',
        'page.gallery' => 'Galeria',
        'page.logistics' => 'Sherbimet',
        'page.data_bank' => 'Të dhëna',
        'page.news' => 'Risi',
        'page.contact' => 'Kontakt',
        'page.errors' => 'Gabim',
        
        //BUTTONS
        'button.seemore' => 'shiko më shumë',
        
        // PAGES
        'home.newProducts' => 'Produktet e reja',
        'home.testimonies' => 'Opinione nga klientet',
        'home.partners' => 'Partneret tane',
        'home.news' => 'Njoftimet e fundit',
        'home.h1' => 'Furnizime spitali',
        'home.h2' => 'Pajisje Mjekësore',
        'home.h3' => 'Kujdesi Kozmetik',
        'home.about1' => 'Ne nuk jemi vetëm një furnizues, por një partner për çdo nevojë të strukturës suaj.',
        'home.about2' => 'Ne kemi zgjedhur një listë partneresh që ka dorëzuar vazhdimisht prej vitesh, produkte me cilësi të shkëlqyeshme.',
        'home.about3' => 'Për të gjithë ata që kërkojnë shërbime të klasit të parë që kombinojnë përdorimin e teknikave më të fundit.',
        'contact.visit-us' => 'NA VIZITONI',
        'contact.contact' => 'KONTAKT',
        'contact.input.name' => 'Emri*',
        'contact.input.email' => 'Email*',
        'contact.input.subject' => 'Subjekti',
        'contact.input.message' => 'Mesazhi*',
        'contact.button.send' => 'Dërgo',
        'products.products-categories' => 'KATEGORITË',
        'products.product-description' => 'PËRSHKRIMI PRODUKTIT',
        'products.product-details' => 'DETAJET E PRODUKTIT',
        'products.related-products' => 'PRODUKTE TË NGJASHËM',
        'products.product-details-category' => 'Kategoria',
        'products.product-details-date' => 'Data',
        'products.search' => 'Nuk u gjenden produkte me keto terma !',
        'products.search2' => 'Provoni te kerkoni me terma te ndryshme.',
        'products.back' => 'Kthehu tek kreu',
        'articles.article-description' => 'PËRSHKRIMI',
        
        // FOOTER
        'footer.contact' => 'KONTAKT',
        'footer.recent-products' => 'PRODUKTE TE FUNDIT',
        'footer.links' => 'MENU',
        'footer.social' => 'RRJETE SOCIALE'
    
    ]

?>