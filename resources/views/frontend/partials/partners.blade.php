<!-- Partners Section-->
<section id="our-clients" class="section type2">
    <!--<div class="subpage-title type2">-->
    <!--    <h5>{{ Lang::get('translations.home.partners') }}</h5>-->
    <!--    <p class="sep">&nbsp</p>-->
        <!-- Controls -->
        <!--<div class="controls"> <span id="partners-prev" class="prev"><i class="fa fa-angle-left"></i></span> <span id="partners-next" class="next"><i class="fa fa-angle-right"></i></span> </div>-->
    <!--</div>-->
    <div class="row flush">
        <div class="col-md-12 text-center">
            <h1 class="cbox-title">{{ Lang::get('translations.home.partners') }}</h1></div>
        </div>
       <!--<span id="partners-prev" class="prev"><i class="fa fa-angle-left"></i></span>-->
       <!--<span id="partners-next" class="next"><i class="fa fa-angle-right"></i></span>-->
        <div class="container">
            <!--<div id="caroufredsel-partners-container">-->
            <div class="row" id="partners-container">
            <div class="col-xs-12 col-sm-6 col-md-3 client-wrapper">
                <a href="https://www.vygon.com/" target="_blank"><img class="img-responsive" src="img/partners/vygon.png" alt="Client Image"></a>
            </div>
            <!-- /.client-wrapper -->
            <div class="col-xs-12 col-sm-6 col-md-3 client-wrapper">
                <a href="https://in.erbe-med.com/in-en/" target="_blank"><img class="img-responsive" src="img/partners/erbe.png" alt="Client Image"></a>
            </div>
            <!-- /.client-wrapper -->
            <div class="col-xs-12 col-sm-6 col-md-3 client-wrapper">
                <a href="http://www.efer.com/" target="_blank"><img class="img-responsive" src="img/partners/efer.png" alt="Client Image"></a>
            </div>
            <!-- /.client-wrapper -->
            <div class="col-xs-12 col-sm-6 col-md-3 client-wrapper">
                <a href="http://www.medster.com.tr/" target="_blank"><img class="img-responsive" src="img/partners/medster.png" alt="Client Image"></a>
            </div>
            <!-- /.client-wrapper -->
            <div class="col-xs-12 col-sm-6 col-md-3 client-wrapper">
                <a href="http://www.kimpailac.com/tr/" target="_blank"><img class="img-responsive" src="img/partners/kimpa.jpg" alt="Client Image"></a>
            </div>
            <!-- /.client-wrapper -->
            <div class="col-xs-12 col-sm-6 col-md-3 client-wrapper">
                <a href="http://www.erpamedikal.com/" target="_blank"><img class="img-responsive" src="img/partners/erpa.svg" alt="Client Image"></a>
            </div>
             <!-- /.client-wrapper -->
            <div class="col-xs-12 col-sm-6 col-md-3 client-wrapper">
                <a href="http://skintact.com/" target="_blank"><img class="img-responsive" src="img/partners/skintact.png" alt="Client Image"></a>
            </div>
            <!-- /.client-wrapper -->
            <div class="col-xs-12 col-sm-6 col-md-3 client-wrapper">
                <a href="http://www.maquet.com/" target="_blank"><img class="img-responsive" src="img/partners/maquet.png" alt="Client Image"></a>
            </div>
            <!-- /.client-wrapper -->
            <!--<div class="col-xs-12 col-sm-4 col-md-3 client-wrapper">-->
            <!--    <a href="http://www.polymedicure.com/"><img class="img-responsive" src="img/partners/polymed.png" alt="Client Image"></a>-->
            <!--</div>-->
            <!-- /.client-wrapper -->
            <div class="col-xs-12 col-sm-6 col-md-3 client-wrapper">
                <a href="http://www.trimpeks.com/en/" target="_blank"><img class="img-responsive" src="img/partners/trimpekslogo.png" alt="Client Image"></a>
            </div>
            <!-- /.client-wrapper -->
            <div class="col-xs-12 col-sm-6 col-md-3 client-wrapper">
                <a href="http://www.jotec.com/" target="_blank"><img class="img-responsive" src="img/partners/jotec.png" alt="Client Image"></a>
            </div>
            <!-- /.client-wrapper -->
            <div class="col-xs-12 col-sm-6 col-md-3 client-wrapper">
                <a href="http://www.brochemedikal.com/" target="_blank"><img class="img-responsive" src="img/partners/broche.png" alt="Client Image"></a>
            </div>
        </div>
        </div>
        <!-- /#caroufredsel-clients-container -->
    </div>
    <!-- /.row -->
</section>