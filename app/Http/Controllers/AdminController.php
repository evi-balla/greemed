<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \App\Category as Category;
use \App\SubCategory as SubCategory;
use \App\Product as Product;
use \App\Gallery as Gallery;
use Exception;
use Validator;
use DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $categories = Category::all();
        $sub_categories = SubCategory::all();
        $products = Product::get()->take(10);
        
        $data = [
            'categories' => $categories,
            'sub_categories' => $sub_categories,
            'products' => $products,
        ];
        
        
        return view('backend.index', $data);
    }
    
    
    public function categories()
    {
        $categories = Category::all();
        
        $data = [
            'categories' => $categories
        ];
        
        return view('backend.categories', $data);
    }
    
    
    public function subCategories()
    {
        $categories = Category::all();
       
        
        $data = [
            'categories' => $categories,
        ];
        
        return view('backend.sub_categories', $data);
    }
    
    public function articles()
    {
        return view('backend.articles');
    }
    
    public function employeeOfMonth()
    {
        return view('backend.employee_of_month');
    }
    
    public function products()
    {
        $sub_categories = SubCategory::all();
        
        $data = [
            'sub_categories' => $sub_categories
        ];
        
        return view('backend.products', $data);
    }
    
    public function galleries()
    {
        $galleries = Gallery::all();
        
        $data = [
            'galleries' => $galleries
        ];
        
        return view('backend.galleries', $data);
    }

    
}
