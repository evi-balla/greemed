@extends('frontend.layouts.main') 
@section('nav') 
@include('frontend.partials.nav') 
@stop 
@section('page_title')
@include('frontend.partials.page_title') 
@stop 
@section('main_container')
<style type='text/css'>@-webkit-keyframes uil-default-anim { 0% { opacity: 1} 100% {opacity: 0} }@keyframes uil-default-anim { 0% { opacity: 1} 100% {opacity: 0} }.uil-default-css > div:nth-of-type(1){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.5s;animation-delay: -0.5s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(2){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.4166666666666667s;animation-delay: -0.4166666666666667s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(3){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.33333333333333337s;animation-delay: -0.33333333333333337s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(4){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.25s;animation-delay: -0.25s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(5){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.16666666666666669s;animation-delay: -0.16666666666666669s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(6){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.08333333333333331s;animation-delay: -0.08333333333333331s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(7){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0s;animation-delay: 0s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(8){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.08333333333333337s;animation-delay: 0.08333333333333337s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(9){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.16666666666666663s;animation-delay: 0.16666666666666663s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(10){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.25s;animation-delay: 0.25s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(11){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.33333333333333337s;animation-delay: 0.33333333333333337s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(12){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.41666666666666663s;animation-delay: 0.41666666666666663s;}.uil-default-css { position: relative;background:none;width:200px;height:200px;}
    .loader {
        position: fixed;
        top: 20%;
        transform: translateY(-50%);
        z-index: 999;
        left: 0;
        right: 0;
    } 
</style>

<div class="loader hidden">
        <div class='uil-default-css' style='transform:scale(0.4);position: absolute;left: 0;right: 0;text-align: center;
    margin: 0 auto;'><div style='top:80px;left:93px;width:14px;height:40px;background:#bf382a;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#bf382a;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#bf382a;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#bf382a;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#bf382a;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#bf382a;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#bf382a;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#bf382a;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#bf382a;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#bf382a;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#bf382a;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'></div><div style='top:80px;left:93px;width:14px;height:40px;background:#bf382a;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
        </div>
    </div>

<div class="row">
    <div class="col-xs-12">
        <div class="form">
            <input type="text" name="string" class="form-control" id="searchProd" placeholder="search product"/>
        </div>
    </div>    
</div>

<div class="row">
    @if(empty($categories))
    <div class="col-xs-12">
        <h1>No products found</h1>
    </div>
    @else
    <div class="col-xs-12 col-md-4">
        <section class="section type2">
            <div class="subpage-title">
                <h5>{{ Lang::get('translations.products.products-categories') }}</h5>
            </div>
            
            <div class="panel-group" id="accordion">
            @foreach($categories as $category)
            <div class="panel panel-default">
                    <div class="panel-heading accordion-toggle category-name collapsed" category-id="{{ $category->id }}"  data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $category->id }}">
                        <h4 class="panel-title" style="cursor: pointer;">
                            <a class=""  >
                                {{ $category->name }}
                            </a>
                        </h4>
                    </div>
                    <div id="collapse{{ $category->id }}" class="panel-collapse collapse">
                        <div class="panel-body panel-sub-cat">
                            @foreach($category->subCategories as $subCategory)
                            <div class="list-group">
                                <li sub_category_id="{{ $subCategory->id }}" class="list-group-item sub-category">
                                    <span class="badge">{{ count($subCategory->products) }}</span>
                                    {{ $subCategory->name }}
                                </li>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>    
            @endforeach
            </div>

            
        </section>
    </div>
    <div class="col-xs-12 col-md-8">
    
    <section class="portfolio section type3">
        <div class="row">
            <div class="not-found hidden" id="notFound">
                <div class="error-content">
                    <h3>{{ Lang::get('translations.products.search') }}</h3>
                    <p class="lead">{{ Lang::get('translations.products.search2') }}</p>
                    <a href="/" class="btn adv-color squre">&laquo; &nbsp; {{ Lang::get('translations.products.back') }}</a>
                </div>
            </div>
            <div class="portfolio-container products-container" style="margin-right: -1px;">
            </div>
             <div class="col-md-12 text-center">
                <ul class="pagination" id="paginationUl">
                    <li class="prev" data-page="1"><a href="javascript:void(0);">«</a></li>
                    <li class="next" data-page="1"><a href="javascript:void(0);">»</a></li>
                </ul>
             </div>
            <!-- /.portfolio-container -->
        </div>
        <!-- /.row -->
    </section>
    </div>
    
    <script type="text/javascript">
    
    // send csrf token on every ajax request
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    var category_id, sub_category_id;
    // Isotope Portfolio
    var $container = $('.products-container');
    var $filter = $('.products-filter');
    
    $(window).load(function () {
        
        var medDevices = localStorage.getItem('medicalDevices')
        var cosCare  = localStorage.getItem('cosmeticCare')
        var searchValue = localStorage.getItem('searchValue')
        
        var first_category = "{{ $first_category ? $first_category->id : 0 }}";
        
        setTimeout(function(){
            if(medDevices === 'true') {
                $(document).find('[category-id="37"]').click();
            }
            else if (cosCare === 'true') {
                    $(document).find('[category-id="39"]').click();
            }
            else if(first_category !== 0 ) {
                $(document).find("[category-id='"+parseInt(first_category)+"']").click();
            }
            
            if(searchValue.length !== 0) {
                var e = jQuery.Event("input");
                e.which = 32;
                $('#searchProd').focus().val(searchValue).trigger(e);
            }
            
        }, 100);
        
        // Initialize Isotope
        $container.isotope({
            itemSelector: '.portfolio-item-wrapper'
        });
        $('.products-filter li').click(function () {
            var selector = $(this).attr('data-filter');
            $container.isotope({ filter: selector });
            return false;
        });
        $filter.find('li').click(function () {
            $filter.find('li').removeClass('active');
            $(this).parent().addClass('active');
            $(this).addClass('active');
        });
    });
    
    $(window).bind('beforeunload', function(){
        localStorage.removeItem('searchValue');
    });

    $(document).on('input', '[name="string"]', function(e) {
        var string = $(this).val();
        
        if(string.length > 0 && string.length < 3) {
            return;
        }
        $('.loader').removeClass('hidden');
        setTimeout(function() {
            $.ajax({
                url: '/api/products/search?string=' + string,
                method: 'get',
                success: function(res) {
                  if (res.message == 'OK') {
                      if(res.result.length === 0) {
                          $('#notFound').removeClass('hidden')
                          $('#paginationUl').addClass('hidden')
                          $('.loader').addClass('hidden');
                          updateGrid(res.result);
                          $('#overlay').addClass('hidden'); 
                      } else {
                        $('#notFound').addClass('hidden')
                        $('#paginationUl').removeClass('hidden')
                        $('.loader').addClass('hidden');
                        updateGrid(res.result);
                        $('#overlay').addClass('hidden');   
                      }
                  }
                  else {
                    $('#overlay').addClass('hidden');
                    swal({
                      title: 'Error',
                      text: res.result,
                      type: 'error'
                    });
                  }
                }
            });
        }, 500)
    });
    
    $(document).on('click', '.category-name', function(e) {
        e.preventDefault();
        category_id = $(this).attr('category-id');
        
        $('.list-group-item').removeClass('active');
        ajaxCall(1);
        
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $container.isotope({ filter: '*' });
            return false;
        } else {
            $('.loader').removeClass('hidden');
            $('.category-name').removeClass('active');
            $(this).addClass('active');
        }
        
    });
    
    $(document).on('click', '.sub-category', function(e) {
        e.preventDefault();
        sub_category_id = $(this).attr('sub_category_id');
        
        $('.list-group-item').removeClass('active');
        ajaxCallSubCategory(1);
        
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $container.isotope({ filter: '*' });
            return false;
        } else {
            $('.loader').removeClass('hidden');
            $('.category-name').removeClass('active');
            $(this).addClass('active');
        }
        
    });
    
    $(document).on('click', '.pagination > li', function(e) {
        e.preventDefault();
        var page = $(this).data('page');
        var type = $(this).parent().attr('type');
        switch (type) {
            case 'category':
                ajaxCall(page);
                break;
            case 'sub-category':
                ajaxCallSubCategory(page)
                break;
        }
    })
    
    function updateGrid(data) {
        
        $container.html('');
        
        $(data).each(function(index, product) {
            var grid_item = "<div class='col-xs-12 col-sm-6 portfolio-item-wrapper " + product.sub_category_id + "'>" +
            "<div class='portfolio-item type2'>" +
                "<div class='portfolio-thumb'>" +
                    "<div style='background-image: url("+ product.image +");' class='product-container-div'>" +
                    "<a href='product/"+product.slug+"' class='image-overlay'></a>" +
                "</div>" +
                "<div class='portfolio-details'>" +
                    // "<div class='portfolio-meta'>" +
                    //     "<span class='portfolio-date'><i class='fa fa-clock-o'></i> <a href='#'>&nbsp; " + product.created_at +  "</a></span>" +
                    // "</div>" +
                    "<h5 class='portfolio-title'><a href='product/"+product.slug+"'>"+ product.name +"</a></h5>" +
                "</div>" +
                "</div>" +
            "</div>";
            
            $container.isotope('insert', $(grid_item));
            $container.isotope('reloadItems');
        });
    }
    
    function ajaxCall(page) {
        $('.loader').removeClass('hidden');
        $('#overlay').removeClass('hidden');
        $('.pagination .next').show();
        $.ajax({
        url: '/admin/get-products-by-category-id?page=' + page,
        method: 'post',
        data: {
          category_id: category_id
        },
        success: function(res) {
          if (res.message == 'OK') {
              $('.loader').addClass('hidden');
              updateGrid(res.result.data);
              $('#overlay').addClass('hidden');
              var pages = res.result.last_page;
              var currentPage = res.result.current_page;
              var lastPage = res.result.last_page;
              var active = '';
              if (currentPage == lastPage) {
                  $('.pagination .next').hide();
              } else {
                  $('.pagination .next').show();
              }
              $('.pagination').attr('type', 'category');
              $('.pagination .prev').data('page', currentPage > 1 ? currentPage - 1 : 1 );
              $('.pagination .next').data('page', currentPage + 1 );
          }
          else {
            $('#overlay').addClass('hidden');
            swal({
              title: 'Error',
              text: res.result,
              type: 'error'
            });
          }
        }
    });

    }
    
    function ajaxCallSubCategory(page) {
        $('.loader').removeClass('hidden');
        $('#overlay').removeClass('hidden');
        $('.pagination .next').show();
        $.ajax({
        url: '/admin/get-products-by-sub-category-id?page=' + page,
        method: 'post',
        data: {
          sub_category_id: sub_category_id
        },
        success: function(res) {
          if (res.message == 'OK') {
              $('.loader').addClass('hidden');
              updateGrid(res.result.data);
              $('#overlay').addClass('hidden');
              var pages = res.result.last_page;
              var currentPage = res.result.current_page;
              var lastPage = res.result.last_page;
              var active = '';
              if (currentPage == lastPage) {
                  $('.pagination .next').hide();
              } else {
                  $('.pagination .next').show();
              }
              $('.pagination').attr('type', 'sub-category');
              $('.pagination .prev').data('page', currentPage > 1 ? currentPage - 1 : 1 );
              $('.pagination .next').data('page', currentPage + 1 );
              
          }
          else {
            $('#overlay').addClass('hidden');
            swal({
              title: 'Error',
              text: res.result,
              type: 'error'
            });
          }
        }
    });

    }
    
</script>
    
    @endif
    
    
</div>


@stop 