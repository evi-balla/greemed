@extends('frontend.layouts.main') 
@section('nav') 
@include('frontend.partials.nav') 
@stop 
@section('page_title')
@include('frontend.partials.page_title') 
@stop 
@section('main_container')

@if(LaravelLocalization::getCurrentLocale() == 'en')
    <!-- /#google-map -->
    <div class="row padding-bottom-50">
        <div class="col-xs-12">
            
            <p>
                <strong>QUALITY POLICY STATEMENT</strong>
            </p>
            <br>
            <p>
                <strong>GreeMed S.A.</strong> aim for the highest standard of quality and intend to retain their position as a leading contracting organisation by continuing to provide client satisfaction and to generate further business opportunities through operating in a safe, efficient and profitable manner.
            </p>
            <br>
            <p>
                The principal aims of company are:
                <ul>
                    <li>To have a real understanding of clients and their business requirements.</li>
                    <li>To strengthen relationships and encourage repeat business with existing clients.</li>
                    <li>To win work not solely on price but through innovation and added value.</li>
                    <li>To continually strive to improve our performance through analysis and continuous improvement of business processes.</li>
                    <li>To develop staff potential through the provision of appropriate training.</li>
                    <li>To engender a positive commitment to quality and create an environment of teamwork and cooperation that enables staff to work effectively.</li>
                </ul>

            </p>
            <br>
            <p>The Directors of the company have specific responsibility for providing the necessary organisation and resources to implement this policy in all aspects of the business.
            </p>
            <br>
            <p>
                However, everyone working for <strong>GreeMed S.A.</strong> has a responsibility for ensuring the quality of their work and that of persons under their supervision. These responsibilities will be defined within the company procedures and project management plans.
            </p>
            <br>
            <p>
                <strong><i>GreeMed – Building Quality Together</i></strong>
            </p>
        </div>
    </div>
    <!-- /.row -->
@elseif(LaravelLocalization::getCurrentLocale() == 'it')
    <!-- /#google-map -->
    <div class="row padding-bottom-50">
        <div class="col-xs-12">
            
            <p>
                <strong>DICHIARAZIONE DELLA POLITICA DI QUALITA</strong>
            </p>
            <br>
            <p>
                <strong>GreeMed S.A.</strong> Ha L’Obiettivo per raggiungere le più alti standard di qualità, che intendono mantenere la loro posizione di organizzazione come principale contraente continuando a fornire la soddisfazione del cliente e di generare ulteriori opportunità commerciali. Attraverso le sue politiche in uso garantisce agli suoi collaboratori la piu’ alta sodisfacione nella qualita e prezzo in modo sicuro, efficiente e redditizio.
            </p>
            <br>
            <p>
                Gli principali obiettivi sono:
                <ul>
                    <li>Per avere una vera comprensione dei clienti e delle loro esigenze di business.</li>
                    <li>Per sviluppare il personale del’azienda attraverso la fornitura di una formazione adeguata.</li>
                    <li>Per rafforzare le relazioni e incoraggiare rapporti commerciali stabili con i client nuovi ed esisten.</li>
                    <li>Per essere competittivi  non solo sul prezzo, ma attraverso l’innovazione che si traduce in  valore aggiunto.</li>
                    <li>Per cercare continuamente di migliorare le nostre prestazioni attraverso l’analisi e il miglioramento continuo dei processi aziendali.</li>
                    <li>Per generare un impegno concreto per la qualità e creare un ambiente di lavoro di squadra e la collaborazione che permette al personale di lavorare in modo efficace.</li>
                </ul>

            </p>
            <br>
            <p>L’Amministratore della Società ha la responsabilità specifica di fornire alla societa le risorse necessarie per attuare tale politica in tutti gli aspetti del business.
            </p>
            <br>
            <p>
                In conclusione, tutti coloro che lavorano per <strong>GreeMed S.A.</strong> hanno la responsabilità di assicurare la qualità del loro lavoro e quello delle persone sotto la loro supervisione. Queste responsabilità sono definite nell’ambito delle procedure aziendali e dei piani di gestione dei progetti.
            </p>
            <br>
            <p>
                <strong><i>GreeMed – Costruire insieme la qualita</i></strong>
            </p>
        </div>
    </div>
    <!-- /.row -->
@else
    <!-- /#google-map -->
    <div class="row padding-bottom-50">
        <div class="col-xs-12">
            
            <p>
                <strong>Deklarata e rregullave të cilësisë.</strong>
            </p>
            <br>
            <p>
                Kompania <strong>Greemed S.A</strong> ka për qëllim të ofrojë  standartin më të lartë të cilësisë  si dhe të vazhdojë të mbajë vendin e saj si një kompani kryesore kontraktuese duke vazhduar të gëzojë klientët e saj si dhe për të krijuar mundësi të tjera biznesi duke vepruar në një mënyrë të sigurtë , efikase dhe fitimprurese.
            </p>
            <br>
            <p>
                Qëllimet kryesore të kompanisë janë:
                <ul>
                	<li>Të kuptojë vërtetë klientet dhe kërkesat  e tyre të biznesit .</li>
                	<li>Të forcojë marrëdhëniet si dhe të vazhdojë  biznesin me klientet aktual</li>
                	<li>Të korrë sukses  jo vetëm për shkak të çmimit  por edhe për shkak të  risive  dhe vlerës.</li>
                	<li>Vashdimisht të përpiqet  të përmirësojë performancën   duke analizuar dhe përmirësuar  vazhdimisht  proçesin  e mënyrës së të bërit biznes.</li>
                	<li>Të zhvillojë potencialin e stafit duke ofruar trajnimin e përshtatshëm</li>
                	<li>Ti përkushtohet më tepër cilësisë si edhe të krijojë një ambjent pune në skuadër në mënyrë që stafi të jetë më produktiv.</li>
                </ul>

            </p>
            <br>
            <p>Drejtuesit e kompanisë kanë për detyrë  të sigurojnë organizimin  e duhur si  dhe burimet për të zbatuar këtë rregull në cdo aspekt të biznesit
            </p>
            <br>
            <p>
                Megjithatë, çdokush që punon në <strong>Greemed S.A</strong> ka për detyrë të ofrojë cilësi të mirë  të punës së tyre si dhe të personave që kanë nën mbikqyrje. Këto përgjegjësi do të përcaktohen në kuadër të procedurave të kompanisë dhe planeve të menaxhimit të projektit.
            </p>
            <br>
            <p>
                <strong><i>GreeMed – Së bashku ndërtojmë cilësinë</i></strong>
            </p>
        </div>
    </div>
    <!-- /.row -->
@endif

@stop 