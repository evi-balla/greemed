@extends('frontend.layouts.main') 
@section('nav') 
@include('frontend.partials.nav') 
@stop 
@section('page_title')
@include('frontend.partials.page_title') 
@stop 
@section('main_container')
<div class="container-contact">
    <section class="section google-map type2">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d997.1849892985247!2d19.79910012700295!3d41.319331649305!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNDHCsDE5JzA5LjYiTiAxOcKwNDcnNjAuMCJF!5e1!3m2!1sen!2s!4v1455364337076"
        width="600" height="450" frameborder="0" style="border:0"></iframe>
    </section>
    <!-- /#google-map -->
    <div class="row padding-bottom-50">
        <div class="col-md-4">
            <section class="section type2">
                <div class="subpage-title">
                    <h5><i class="fa fa-compass"></i> {{Lang::get('translations.contact.visit-us')}}</h5> 
                </div> 
                <address>
                    <strong>
                        GREEMED S.A 
                        <br>
                        Medical Solutions 
                        <br>
                        Your Health Partner
                    </strong>
                    <br>
                    Adress: Rr. “Ramazan Demneri” Nd. 1 H.
                    <br>
                    Njesia Bashkiake Nr.5 Kodi Postar 1022
                    <br>
                    Tirana – Albania
                    <br>
                </address> 
            </section>
            <section class="section type2">
                <div class="subpage-title">
                    <h5>
                        <i class="fa fa-phone"></i> 
                        {{Lang::get('translations.contact.contact')}}
                    </h5> 
                </div> 
                <address>
                    Tel/Fax: +355 (0) 42 23 56 47<br>
                    Fax: (123) 456-7890<br>
                    <a href="mailto:#">info@greemed.eu</a> <br>
                    Web: www.greemed.eu
                </address>
            </section>
        </div>
        <div class="col-md-8">
            <section class="section type2">
                <form class="contact-form">
                    <div class="">
                        <div class="col-md-4">
                            <input class="form-control" name="name" id="name" placeholder="{{Lang::get('translations.contact.input.name')}}" type="text">
                        </div>
                        <div class="col-md-4">
                            <input class="form-control" name="email" id="email" placeholder="{{Lang::get('translations.contact.input.email')}}" type="email">
                        </div>
                        <div class="col-md-4">
                            <input class="form-control" name="subject" id="subject" placeholder="{{Lang::get('translations.contact.input.subject')}}" type="text">
                        </div>
                    </div>
                    <!-- row-fluid -->
                    <div class="col-md-12">
                        <textarea class="form-control" name="message" id="message" placeholder="{{Lang::get('translations.contact.input.message')}}" rows="5"></textarea>
                    </div>
                    <div class="col-md-12 text-center">
                        <button id="submit" class="btn adv-color">{{Lang::get('translations.contact.button.send')}}</button>
                    </div>
                </form>
                <!-- /.contact-form -->
            </section>
        </div>
    </div>
    <!-- /.row -->
</div>

<script type="text/javascript">
    /* global $ */
    
    // send csrf token on every ajax request
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    var API_BASE = 'http://greemed.eu/';

    $('#submit').on('click', function(e) {
        e.preventDefault();
        let name = $('#name').val();
        let email = $('#email').val();
        let subject = $('#subject').val();
        let message = $('#message').val();
        let _token = '{{ csrf_token() }}';
        
        $.ajax({
            url: API_BASE + 'api/sendMail',
            method: 'POST',
            data: {
                name: name,
                email: email,
                subject: subject,
                message: message,
                _token: _token
            },
            error: function(res) {
                alert('Error sending email');
            },
            success: function(res) {
                alert('Email was sent successfully');
            }
        })
    });
    
</script>

@stop 