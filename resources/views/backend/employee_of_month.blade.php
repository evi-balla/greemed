@extends('backend.layouts.main')
@section('main_content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Employee of month</h3> </div>
      <!-- /.box-header -->
      <!-- form start -->
      <form role="form" name="employeeForm">
        <div class="box-body">
          <div class="form-group">
            <label for="description">Text</label>
            <textarea rows="4" name="text" id="text" class="form-control">
            </textarea>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button id="btnCreateEmployee" type="submit" class="btn btn-success">Save</button>
          <button id="btnDeleteEmployee" type="submit" class="btn btn-danger hidden">Delete</button>
        </div>
      </form>
    </div>
  </div>
    
  <style type="text/css">
    .pagination {
        margin: 0;
    }
    #image_thumb {
        max-height: 100px;
        margin: 10px;
        padding: 5px;
        border: solid 1px #ddd;
    }
  </style>
    
  <script>
  
    // send csrf token on every ajax request
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    
    /* global swal, CKEDITOR */
    
    // array of categories to manipulate
    var articles = [];
    
    // run
    getEmployeeOfMOnth();
    
    $(document).ready(function() {
      CKEDITOR.replace('text');
    });
    
    // form create button
    $('#btnCreateEmployee').click(function(event) {
      event.preventDefault();
      
      $('#overlay').removeClass('hidden');
      
      var $text = CKEDITOR.instances.text.getData();
      
      $.ajax({
        url: '/admin/create-employee-of-month',
        method: 'POST',
        data: { text: $text },
        success: function(res) {
          if (res.message == 'OK') {
              CKEDITOR.instances.text.setData(res.result.text);
            $('#overlay').addClass('hidden');
            $('#btnDeleteEmployee').removeClass('hidden');
            swal({
              title: 'Employee saved successfully',
              type: 'success'
            });
          }
          else {
            swal({
              title: 'Error',
              text: res.result,
              type: 'error'
            });
          }
        }
      });
    });
    
    // action delete button
    $(document).on('click','#btnDeleteEmployee', function(event) {
      
      event.preventDefault();
      
      swal({
        title: "Are you sure?",
        text: "You will not be able to recover this article!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true
      }, function() {
        $('#overlay').removeClass('hidden');
        
        $.ajax({
          url: '/admin/delete-employee-of-month',
          method: 'POST',
          success: function(res) {
            if (res.message == 'OK') {
              $('#overlay').addClass('hidden');
              $('#btnDeleteEmployee').addClass('hidden');
              CKEDITOR.instances.text.setData('');
              swal({
                title: 'Employee deleted successfully',
                type: 'success'
              });
            }
            else {
              $('#overlay').addClass('hidden');
              swal({
                title: 'Error',
                text: res.result,
                type: 'error'
              });
            }
          }
        });
      });
    });
    
    // get all categories 
    function getEmployeeOfMOnth() {
      $('#overlay').removeClass('hidden');
      $.ajax({
        url: '/admin/get-employee-of-month',
        method: 'POST',
        success: function(res) {
          if (res.message == 'OK') {
            $('#overlay').addClass('hidden');
            if(res.result != null) {
                CKEDITOR.instances.text.setData(res.result.text);
            } else {
                $('#btnDeleteEmployee').addClass('hidden');
            }
          }
          else {
            swal({
              title: 'Error',
              text: res.result,
              type: 'error'
            });
          }
        }
      });
    }
    
  </script>
    
</div>
@stop