<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \App\EmployeeOfMonth as Employee;
use Exception;
use Validator;

class EmployeeController extends BaseController
{

    public function get()
    {
        try {
            
            $employee = Employee::first();
            
            return $this->doResponse('OK', $employee, 200);

            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }

    public function store(Request $request)
    {
        try {
            
            $validator = Validator::make(
                $request->all(),
                [
                    'text' => 'required'
                ]
            );
            
            if($validator->fails()) {
                return $this->doResponse('ERR', $validator->errors(), 401);
            }
            
            if(Employee::first()) {
                $employee = Employee::first();
                $employee->text = $request->input('text');
                $employee->save();
            } else {
                $employee = new Employee;
                $employee->text = $request->input('text');
                $employee->save();
            }
            
            return $this->doResponse('OK', $employee, 200);
            
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }

    public function destroy(Request $request)
    {
        try {
            
            if(Employee::first()) {
                Employee::first()->delete();
            } else {
                return $this->doResponse('ERR', 'No found', 400);
            }
            
            return $this->doResponse('OK', 'Employee deleted successfully', 200);
            
            
        } catch (Exception $e) {
            return $this->doResponse('ERR', $e->getMessage(), 500);
        }
    }
    
}
