<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Gallery extends Model implements SluggableInterface
{
    protected $table = 'galleries';
    
    protected $fillable = ['name', 'slug'];
    
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
    ];
    
    public function images() {
        return $this->hasMany('App\GalleryImages');
    }
}
