<header id="masthead" class="site-header" role="banner">
    <nav class="navbar navbar-default navbar-static-top navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a class="navbar-brand" href="/"> <img alt="Brand" src="/img/greemed_logo.png"> </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse"> 
                <ul class="nav navbar-nav navbar-right">
                    <?php 
                        $current = Route::getCurrentRoute()->getPath();
                    ?>
                    <li class="{{ strlen($current) == 2 ? 'active' : ''  }}"><a href="/">{{ Lang::get('translations.menu.home') }}</a></li>
                    <li class="{{ strpos($current, 'about') ? 'active' : '' }}" ><a href="/about">{{ Lang::get('translations.menu.about') }}</a></li>
                    <li class="{{ strpos($current, 'product') ? 'active' : '' }}" ><a href="/products">{{ Lang::get('translations.menu.products') }}</a></li>
                    <li class="{{ strpos($current, 'quality') ? 'active' : '' }}" ><a href="/quality">{{ Lang::get('translations.menu.quality') }}</a></li>
                    <!--<li class="{{ strpos($current, 'gallery') ? 'active' : '' }}" ><a href="/gallery">{{ Lang::get('translations.menu.gallery') }}</a></li>-->
                    <!--<li class="{{ strpos($current, 'logistics') ? 'active' : '' }}" ><a href="/logistics">{{ Lang::get('translations.menu.logistics') }}</a></li>-->
                    <!--<li class="{{ strpos($current, 'data-bank') ? 'active' : '' }}" ><a href="/data-bank">{{ Lang::get('translations.menu.data_bank') }}</a></li>-->
                    <li class="{{ strpos($current, 'news') ? 'active' : '' }}" ><a href="/news">{{ Lang::get('translations.menu.news') }}</a></li>
                    <li class="{{ strpos($current, 'contact') ? 'active' : '' }}" ><a href="/contact">{{ Lang::get('translations.menu.contact') }}</a></li>
                    <li class="dropdown"> 
                        <a href="#" class="dropdown-toggle languages" data-toggle="dropdown">
                        @if(LaravelLocalization::getCurrentLocale() == 'en')
                        <img class="flags" src="/img/gb.svg" height="15"></img>
                        @elseif(LaravelLocalization::getCurrentLocale() == 'it')
                        <img class="flags" src="/img/it.svg" height="15"></img>
                        @else
                        <img class="flags" src="/img/al.svg" height="15"></img>
                        @endif
                        <!--<i class="fa fa-angle-double-down"></i>-->
                        </a>
                        <ul class="dropdown-menu" style="max-width: 100px !important;">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                          <li>
                              <a rel="alternate" hreflang="{{$localeCode}}" href="{{ LaravelLocalization::getLocalizedURL($localeCode) }}">
                              
                                @if($localeCode == 'en')
                                <img class="flags" src="/img/gb.svg" height="15"></img>
                                @elseif($localeCode == 'it')
                                <img class="flags" src="/img/it.svg" height="15"></img>
                                @else
                                <img class="flags" src="/img/al.svg" height="15"></img>
                                @endif
                              
                              {{{ $properties['name'] }}}
                          </a></li>
                        @endforeach
                        </ul>
                    </li>
                    <li><input type="text" name="searchBox" placeholder="search" class="form-control" id="searchBox" /></li>
                </ul>
                <!-- /.nav -->
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- /.navbar -->
</header>

<script type="text/javascript">
    $(document).on("keypress", "input", function(e){
        if(e.which == 13){
            var inputVal = $(this).val();
            localStorage.setItem('searchValue', inputVal);
            window.location.href = '/products';
        }
    });
</script>