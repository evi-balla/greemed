<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeOfMonth extends Model
{
    protected $table = 'employee_of_month';
    
    protected $fillable = ['text'];
}
